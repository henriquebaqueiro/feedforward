`timescale 1ns/100ps
module tb_my_net_cam;

parameter WHOLE = 10;
parameter FRACT = 10;
parameter N = 8;


reg clk;
initial clk = 1'b0;
always #(1) clk = ~clk;

reg rst_n;
initial begin
  rst_n = 1;
  @(negedge clk);
  rst_n = 0;
  @(negedge clk);
  rst_n = 1;
end

reg clk_25;         // 25MHz
always @(posedge clk or negedge rst_n) begin
  if (!rst_n) clk_25 <= 1'b0;
  else        clk_25 <= !clk_25;
end

// reg valid_in;
// initial begin
//   valid_in = 1'b0;
//   repeat (2) @(negedge clk_25);
//   valid_in = 1'b1;
//   repeat (2) @(negedge clk_25);
//   valid_in = 1'b0;

//   @(negedge valid_out);
//   $stop;
// end


wire vga_clk;
wire vga_sync;
wire vga_blank;
wire vga_hsync;
wire vga_vsync;
wire [7:0] vga_R;
wire [7:0] vga_G;
wire [7:0] vga_B;

wire cam_xclk;
wire cam_rst_n;
wire cam_pwdn_p;

reg cam_pclk;       // Cam Pixel Clock (same frequency as cam_xclk)
reg cam_vsync;
reg cam_href;

reg cam_d0, cam_d1, cam_d2, cam_d3, cam_d4, cam_d5, cam_d6, cam_d7;

wire [3:0] net_classification;
wire valid_out;

reg switch;

initial begin
  cam_pclk  = 1'b0;
  cam_vsync = 1'b0;
  cam_href  = 1'b0;
end



my_net_cam #(
  .FRACT(FRACT),
  .WHOLE(WHOLE),
  .N(N)
)
my_net_cam_inst
(
  .clk(clk), // 50 MHz
  .rst_n(rst_n),

  // VGA Interface
  .vga_clk(vga_clk),
  .vga_sync(vga_sync),      // always high
  .vga_blank(vga_blank),    // always high

  .vga_hsync(vga_hsync),
  .vga_vsync(vga_vsync),

  .vga_R(vga_R), // [7:0]
  .vga_G(vga_G), // [7:0]
  .vga_B(vga_B), // [7:0]

  // Camera Interface
  .cam_xclk(cam_xclk),      // Cam System Clock (25MHz)
  .cam_rst_n(cam_rst_n),    // always high (active low)
  .cam_pwdn_p(cam_pwdn_p),  // always low (active high)

  .cam_pclk(cam_pclk),      // Cam Pixel Clock (same frequency as cam_xclk)
  .cam_vsync(cam_vsync),
  .cam_href(cam_href),

  .cam_d0(cam_d0),
  .cam_d1(cam_d1),
  .cam_d2(cam_d2),
  .cam_d3(cam_d3),
  .cam_d4(cam_d4),
  .cam_d5(cam_d5),
  .cam_d6(cam_d6),
  .cam_d7(cam_d7),

  // Network Interface
  //.valid_in(valid_in),
  .net_classification(net_classification), // [3:0]
  .valid_out(valid_out),

  // Test pins
  .negative(1'b0),
  .switch(switch)
);

endmodule