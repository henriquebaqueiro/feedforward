###############################################################################
######################### PROJECT DECISION PARAMETERS #########################
###############################################################################

WHOLE = 2
FRACT = 13 # be shure FRACT >= pixel_bits (check this)
n = 9 # LUT bit resolution (n of bits activation function will be represented)

sizes = [784, 30, 30, 10]

# FIXED PARAMETERS
pixel_bits = 8

###############################################################################
############################### PYTHON IMPORTS ################################
###############################################################################

from math import *
import numpy as np
from datetime import datetime
from bitstring import BitArray  # pip install bitstring
                                # (not in anaconda defaults channel)
import matplotlib.pyplot as plt

# import mnist_loader
# training_data, validation_data, test_data = mnist_loader.load_data_wrapper()

###############################################################################
############################ LOAD TRAINED CLASSIFIER ##########################
###############################################################################

from os.path import expanduser
home = expanduser("~")

import sys
sys.path.insert(0, home + '/feedforward/neural-networks-and-deep-learning/src')
from network2 import *

net=load(home + '/feedforward/trained_net/784-30-30-10')

###############################################################################
######################## HELPER FUNCTIONS AND VARIABLES #######################
###############################################################################

# Full word length
fwl = 1 + WHOLE + FRACT

# Find max and min biases
bias_max = 0
bias_min = 0
for biases in net.biases:
    for bias in biases:
        if (bias[0] < bias_min):
            bias_min = bias[0]
        if (bias[0] > bias_max):
            bias_max = bias[0]

# Find max and min weights
weight_max = 0
weight_min = 0
for layer_weights in net.weights:
    for weights in layer_weights:
        for weight in weights:
            if (weight < weight_min):
                weight_min = weight
            if (weight > weight_max):
                weight_max = weight

# How many bits does an integer needs
def how_many_bits(n):
    return int(ceil(log(n+1,2)))

input_neurons = sizes[0]
output_neurons = sizes[len(sizes)-1]

out_layer = len(sizes)-1
out_bits = how_many_bits(output_neurons)

def sigmoid(x):
    return 1/(1+e**-x)

# Recover the float number from a signed binary string
def bin2dec(binary_str, fraction_bits):
    num_back = BitArray(bin = binary_str).int*(2**-fraction_bits)
    return num_back

###############################################################################
################################# LUT DESIGN ##################################
###############################################################################
    
# This LUT design approach is described in:
# HIMAVATHI, S.; ANITHA, D.; MUTHURAMALINGAM, A. Feedforward neural network
# implementation in fpga using layer multiplexing for effective resource
# utilization. IEEE Transactions on Neural Networks, IEEE, v. 18, n. 3,
# P. 880-888, 2007.

# Determine the range of input (x) for which the range of output is between
# (2^-n) and (1-2^-n).
lower_limit = -log(((2**n)-1),e)
upper_limit =  log(((2**n)-1),e)
delta_x = log((0.5+(2**-n))/(0.5-(2**-n)),e)

def x_slice(delta_x):     # Improvement: we want the slice to be a power of 2
    i=1.0                 # so that the actual value of the weighted inputs
    while (i > delta_x):  # will be the LUT address.
        i = i/2
    return i

new_delta_x = x_slice(delta_x)
min_lut_entries = (upper_limit-lower_limit)/new_delta_x
lut_entries = int(2**ceil(log(min_lut_entries,2)))
lut_addr_bits = int(log(lut_entries,2))
input_range = lut_entries*new_delta_x

# Computation of pre-LUT
x_array = []
y_array = []
for i in range(lut_entries):
    x = -input_range/2 + new_delta_x*i
    y = sigmoid(x)
    x_array.append(x)
    y_array.append(y)

# Reorder to consider negative x values inside LUT index
x_array_reordered = x_array[:]
y_array_reordered = y_array[:]
for i in range (int(lut_entries/2)):
    x_array_reordered.insert(0,x_array_reordered.pop())
    y_array_reordered.insert(0,y_array_reordered.pop())

#fig = plt.figure()
#plt.title('LUT organization')
#ax = fig.gca()
#ax.set_xlabel('x_axis')
#ax.set_ylabel('y_axis')
#ax.set_xticks(np.arange(-(input_range/2 + 1), (input_range/2 + 1), 1))
#ax.set_yticks(np.arange(0, 1.1, 0.1))
#plt.scatter(x_array, y_array_reordered)

# CREATE .mif FILE for ROM initialization
# (Convert it to .hex using QUARTUS afterwards)
rom_init_path = ('../modules/memories/activationROM_%dx%dbits/' 
                 % (lut_entries, n))
rom_init_filename = ('activationROM_%dx%dbits_init.mif'% (lut_entries, n))
f = open(rom_init_path + rom_init_filename, 'w')
mif_header = 'WIDTH=%d;\nDEPTH=%d;\nADDRESS_RADIX = UNS;\nDATA_RADIX = UNS;\n\
CONTENT BEGIN\n' % (n, lut_entries)
f.write(mif_header)
for i in range (lut_entries):
    y_int = int(y_array_reordered[i]*(2**n))
    f.write('\t%d\t:\t%d;\n' % (i, y_int))
f.write('END;')
f.close()

# Design considerations
fraction_bits = log(1/new_delta_x,2)
integer_bits  = lut_addr_bits-fraction_bits
print('\nFor proper LUT absciss-index match, consider:\n\
%d bits for integer and \
%d bits for fraction part.' % (integer_bits, fraction_bits))

###############################################################################
############################### VERILOG MODULES ###############################
###############################################################################

    ###########################################################################
    ############################ LAYERS GENERATION ############################
    ###########################################################################

for current_layer in range(1, len(sizes)):

    module_name = 'layer%d' % (current_layer)
    module_filename = module_name + '.v'
    module_path = '../modules/layers/'
    f = open(module_path + module_filename, 'w')
    f.write('// File generated at ' + 
            datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '.' + 2*'\n')
    f.write('module ' + module_name + '\n')
    f.write('#(\n')
    f.write('\t' + 'parameter FRACT = %d,\n' % FRACT)
    f.write('\t' + 'parameter WHOLE = %d' % WHOLE)
    f.write('\n)\n')
    f.write('(\n')
    f.write('\t' + 'input clk,\n')
    f.write('\t' + 'input rst_n,\n')
    f.write('\t' + 'input [%d:0] layer_in,\n' % (n-1))
    f.write('\t' + 'input valid_in,\n')
    
    f.write('\t' + 'output reg [%d:0] mem_index,\n' % (lut_addr_bits-1))
    f.write('\t' + 'input [%d:0] mem_out,\n' % (n-1))
    
    f.write('\t' + 'output reg  [%d:0] layer_out_delayed,\n' % (n-1))
    f.write('\t' + 'output reg delayed_valid_out\n')
    f.write(');\n\n')

        #######################################################################
        ###################### WRITE WEIGHTS AND BIASES #######################
        #######################################################################

    header = '\nwire signed [%s:0] weight [0:%d][0:%d];\n' % (fwl-1, 
                            sizes[current_layer]-1, sizes[0+current_layer-1]-1)
    f.write(header)
    for y in range(len(net.weights[current_layer-1])):
        for z in range(len(net.weights[current_layer-1][y])):
            if ((y < sizes[current_layer]) & (z < sizes[0+current_layer-1])):
                var_name = 'weight[%d][%d]' % (y,z)
                prefix = '%d\'b' % fwl
                fixed = int(net.weights[current_layer-1][y][z]*(2**FRACT))
                #fixed = int(1*(2**FRACT)) # JUST FOR TESTS
                binary = bin(((1<<fwl)-1) & fixed)[2:].zfill(fwl)
                #binary='s12345ABCDEF123456789'
                bh = binary[0] +'_'+ binary[1:WHOLE+1] +'_'+ binary[WHOLE+1:]
                s = 'assign %s = %s%s; //%f\n' % (var_name, prefix, bh, 
                        net.weights[current_layer-1][y][z])
                f.write(s)

    # BIASES
    header = '\nwire signed [%s:0] bias [0:%d];\n' % (fwl-1, 
                            sizes[current_layer]-1)
    f.write(header)
#    for layer in range(len(net.biases)): #for layer in b:
    for neuron in range(len(net.biases[current_layer-1])):
        if (neuron < sizes[current_layer]):  # REVISE THIS!!!
            var_name = 'bias[%d]' % (neuron)
            prefix = '%d\'b' % fwl
            fixed = int(net.biases[current_layer-1][neuron][0]*(2**FRACT))
            #fixed = 10 # JUST FOR TESTS
            binary = bin(((1<<fwl)-1) & fixed)[2:].zfill(fwl)
            bh = binary[0] + '_' + binary[1:WHOLE+1] + '_' + binary[WHOLE+1:]
            s = 'assign %s = %s%s; //%f\n' % (var_name, prefix, bh, 
                    net.biases[current_layer-1][neuron][0])
            f.write(s)
    f.write('\n')

        #######################################################################
        ########################### BIT-EXTENSION #############################
        #######################################################################
    
    if (FRACT > n):
        f.write('\nwire signed [(1+WHOLE+FRACT)-1:0] layer_in_ext = ')
        f.write('{1\'b0, {WHOLE{1\'b0}}, layer_in, {(FRACT-%d){1\'b0}}};\n' 
                  % (n))
    else:
        f.write('\nwire signed [(1+WHOLE+FRACT)-1:0] layer_in_ext = ')
        f.write('{1\'b0, {WHOLE{1\'b0}}, layer_in[%d:(%d-FRACT)]};\n' 
                  % (n-1, n))

        #######################################################################
        ######################### FSM FOR LAYER STATE #########################
        #######################################################################

    f.write('\n// FSM FOR LAYER STATE\n')
    
    # idle + neurons + done
    state_bits_amount = how_many_bits(sizes[current_layer-1]+1) 

    f.write('localparam IDLE\t= %d\'d0,\n' % state_bits_amount)
    for state_number in range(max(sizes[current_layer-1], sizes[current_layer])):
        f.write('          ST_%d= %d\'d%d,\n' 
                % (state_number+1, state_bits_amount, state_number+1))
    f.write('          DONE  = %d\'d%d;\n'
            % (state_bits_amount, state_number+2))
    
    f.write('\nreg [%d:0] state, next_state;\n' % (state_bits_amount-1))
    
    f.write('always @ (posedge clk or negedge rst_n) begin\n')
    f.write('    if (!rst_n) state <= IDLE;\n')
    f.write('    else        state <= next_state;\n')
    f.write('end\n')

    f.write('\nalways @(*) begin\n')
    f.write('    case(state)\n')
    f.write('        IDLE: if (valid_in) next_state = ST_1;\n')
    f.write('              else          next_state = IDLE;\n')
    
    for state_number in range(sizes[current_layer-1]):
        if (state_number != sizes[current_layer-1]-1):
            f.write('        ST_%d: next_state = ST_%d;\n' % 
                    (state_number+1, state_number+2))
        else:
            f.write('        ST_%d: next_state = DONE;\n' % (state_number+1))
    
    f.write('        DONE: next_state = IDLE;\n')
    f.write('        default: next_state = IDLE;\n')
    f.write('    endcase\n')
    f.write('end\n')

        #######################################################################
        ######################### WEIGHT MULTIPLEXING #########################
        #######################################################################

    f.write('\n// WEIGHT MULTIPLEXING\n')
    f.write('reg acc_en;\n')
    
    for neuron in range(sizes[current_layer]):
        f.write('reg signed [(1+WHOLE+FRACT)-1:0] weight_n%d;\n' % neuron)

    f.write('\nalways @(*) begin\n')
    f.write('    case(state)\n\n')
    
    
    def weight_multiplex_enabled_block(state_index):
        f.write('           acc_en = 1\'b1;\n')
        for st in range(sizes[current_layer]):
            f.write('           weight_n%d = weight[%d][%d];\n' 
                    % (st, st, state_index))
        
    def weight_multiplex_disabled_block():
        f.write('           acc_en = 1\'b0;\n')
        for st in range(sizes[current_layer]):
            f.write('           weight_n%d = %d\'d0;\n' % (st, 1+WHOLE+FRACT))
        
    f.write('        IDLE: begin\n')
    weight_multiplex_disabled_block()
    f.write('        end\n\n')
    
    for st in range(sizes[current_layer-1]):
        f.write('        ST_%d: begin\n' % (st+1))
        weight_multiplex_enabled_block(st)
        f.write('        end\n\n')
    
    f.write('        DONE: begin\n')
    weight_multiplex_disabled_block()
    f.write('        end\n\n')   
    
    f.write('        default: begin\n')
    weight_multiplex_disabled_block()
    f.write('        end\n\n')
    
    f.write('    endcase\n')
    f.write('end\n')

        #######################################################################
        ############################# MULTIPLIERS #############################
        #######################################################################

    f.write('\n// MULTIPLIERS\n\n')

    for neuron in range(sizes[current_layer]):
        f.write('wire signed [(1+WHOLE+FRACT)-1:0] wi_n%d;\n' % neuron)
        
    f.write('\n')
    
    for neuron in range(sizes[current_layer]):
        f.write('wire overflow%d;\n' % neuron)
     
    def instantiate_mult(index):
        f.write('\nsigned_fxp_mult\n#(\n')
        f.write('    .FRACT(FRACT),\n    .WHOLE(WHOLE)\n')
        f.write(')\nmult_n%d\n(\n' % index)
        f.write('    .in1(layer_in_ext),\n')
        f.write('    .in2(weight_n%d),\n' % index)
        f.write('    .out(wi_n%d),\n' % index)
        f.write('    .overflow(overflow%d)\n' % index)
        f.write(');\n')
    
    for mult in range(sizes[current_layer]):
        instantiate_mult(mult)
        
        #######################################################################
        ############################ ACCUMULATORS #############################
        #######################################################################

        # acc_extra_bits value is not obvious because one would have to perform
        # all the calculations using the chosen parameters for fixed-point
        # (FRACT and WHOLE) to know among all accumulators which is the highest
        # value possible. This is possible to be done, but for now I will just
        # consider the worst case, which is all the inputs are 1, all bias is
        # the highest trained bias in module and all weights are the highest
        # trained weight in module as well.
        # (TO DO: try more bits and check the classification results)
    acc_extra_bits = how_many_bits(sizes[current_layer]) + 0

    f.write('\n// ACCUMULATORS\n\n')

    for neuron in range(sizes[current_layer]):
        f.write('reg signed [(%d)+(1+WHOLE+FRACT)-1:0] acc_n%d;\n'
                             % (acc_extra_bits, neuron))    
    f.write('\n')
    for neuron in range(sizes[current_layer]):
        f.write('wire msb_acc_n%d = acc_n%d[(%d)+(1+WHOLE+FRACT)-1];\n'
                                            % (neuron, neuron, acc_extra_bits))
    f.write('\n')
    for neuron in range(sizes[current_layer]):
        
        f.write('wire [(%d)+(WHOLE-%d+1)-1:0] trunc_acc_n%d = '
                       % (acc_extra_bits, integer_bits, neuron))
        f.write('acc_n%d[(%d)+(1+WHOLE+FRACT)-1-(1):FRACT+%d];\n' 
                       % (neuron, acc_extra_bits, integer_bits-1))
    f.write('\n')
    
    f.write('\nalways @(posedge clk or negedge rst_n) begin\n')
    f.write('    if (!rst_n) begin\n')
    for neuron in range(sizes[current_layer]):
        f.write('        acc_n%d <= bias[%d];\n' % (neuron, neuron))
    f.write('    end\n')
    
    f.write('    else if (valid_in) begin\n')
    for neuron in range(sizes[current_layer]):
        f.write('        acc_n%d <= bias[%d];\n' % (neuron, neuron))
    f.write('    end\n')
    
    f.write('    else if (acc_en) begin\n')
    for neuron in range(sizes[current_layer]):
        f.write('        acc_n%d <= acc_n%d + wi_n%d;\n'
                % (neuron, neuron, neuron))
    f.write('    end\n')
    f.write('    else begin\n')
    for neuron in range(sizes[current_layer]):
        f.write('        acc_n%d <= acc_n%d;\n' % (neuron, neuron))
    f.write('    end\n')
    f.write('end\n')
    
        #######################################################################
        ############################ LUT INDEX ################################
        #######################################################################
    
    # truncate acc because index has fewer fraction bits
    if (FRACT >=  fraction_bits):
        for neuron in range(sizes[current_layer]):
            f.write('wire [%d:0] index_n%d = ' % (lut_addr_bits-1, neuron))
            f.write('{msb_acc_n%d, acc_n%d[(FRACT-1)+%d:FRACT-%d]};\n'
                    % (neuron, neuron, integer_bits-1, fraction_bits))
    # add zeros to index because it has more fraction bits than acc
    else:
        for neuron in range(sizes[current_layer]):
            f.write('wire [%d:0] index_n%d = ' % (lut_addr_bits-1, neuron))
            f.write('{msb_acc_n%d, acc_n%d[(FRACT-1)+%d:0],{(%d){1\'b0}}};\n'
                    % (neuron, neuron, integer_bits-1, fraction_bits-FRACT))
            
        #######################################################################
        ############################### OVERFLOW ##############################
        #######################################################################
        
    f.write('\n')
    f.write('// if msb=0, only one 1 is enough to point out that a positive ')
    f.write('overflow occured.\n')
    for neuron in range(sizes[current_layer]):
        f.write('wire ovfpos_trunc_acc_n%d = !msb_acc_n%d && (|trunc_acc_n%d);' 
                % (neuron, neuron, neuron))
    f.write('\n\n')
    f.write('// if msb=1, only one 0 is enough to point out that a negative ')
    f.write('overflow occured.\n')
    for neuron in range(sizes[current_layer]):
        f.write('wire ovfneg_trunc_acc_n%d =  ' % (neuron))
        f.write('msb_acc_n%d && !(&trunc_acc_n%d);' % (neuron, neuron))
    f.write('\n\n')

        #######################################################################
        ##################### FSM FOR ACTIVATION FUNCTION #####################
        #######################################################################

    f.write('\n// FSM FOR ACTIVATION FUNCTION\n\n')

    f.write('reg [%d:0] mem_last_state, mem_state, mem_next_state;\n'
            % (state_bits_amount-1))

    f.write('always @(posedge clk or negedge rst_n) begin\n')
    f.write('\t' + 'if (!rst_n) begin\n')
    f.write(2*'\t' + 'mem_state <= IDLE;\n')
    f.write(2*'\t' + 'mem_last_state <= IDLE;\n')
    f.write('\t' + 'end\n')
    f.write('\t' + 'else begin\n')
    f.write(2*'\t' + 'mem_state <= mem_next_state;\n')
    f.write(2*'\t' + 'mem_last_state <= mem_state;\n')
    f.write('\t' + 'end\n')
    f.write('end\n\n')

    f.write('always @(*) begin\n')
    f.write('\t' + 'case(mem_state)\n')
    f.write(2*'\t' + 'IDLE: if (state == DONE) mem_next_state = ST_1;\n')
    f.write(3*'\t' + 'else               mem_next_state = IDLE;\n')
    
    for state_number in range(1, sizes[current_layer]+1):
        if (state_number != sizes[current_layer]):
            f.write(2*'\t' + 'ST_%d: mem_next_state = ST_%d;\n' 
                    % (state_number, state_number+1))
        else:
            f.write(2*'\t' + 'ST_%d: mem_next_state = DONE;\n' 
                    % (state_number))
    
    f.write(2*'\t' + 'DONE:       mem_next_state = IDLE;\n')
    f.write(2*'\t' + 'default:    mem_next_state = IDLE;\n')
    f.write('\t' + 'endcase\n')
    f.write('end\n\n')

        #######################################################################
        ######################### ROM INPUT SELECTION #########################
        #######################################################################

    f.write('// ROM INPUT SELECTION (FROM ACCUMULATORS)\n\n')

    f.write('// reg [%d:0] mem_index;\n' % (lut_addr_bits-1))
    f.write('// reg mem_rd_en;\n')
    f.write('always @(*) begin\n')
    f.write('\t' + 'case(mem_next_state)\n')
    
    def active_state(index):
        f.write(3*'\t' + 'mem_index = index_n%d;\n' % (index))
    
    def inactive_state(m):
        f.write('\t' + 'mem_index = %d\'d0;\n' % (m))
        
    f.write(2*'\t' + 'IDLE:   begin\n')
    inactive_state(lut_addr_bits)
    f.write(2*'\t' + 'end\n')
    
    for st in range(sizes[current_layer]):
        f.write(2*'\t' + 'ST_%d: begin\n' % (st+1))
        active_state(st)
        f.write(2*'\t' + 'end\n')
    
    f.write(2*'\t' + 'DONE:   begin\n')
    #inactive_state(lut_addr_bits)
    
    f.write('\t' + 'mem_index = %d\'d0;\n' % (lut_addr_bits))
    
    f.write(2*'\t' + 'end\n')
    f.write(2*'\t' + 'default:begin\n')
    inactive_state(lut_addr_bits)
    f.write(2*'\t' + 'end\n')  
    
    f.write('\t' + 'endcase\n')
    f.write('end\n\n')



    f.write('reg [%d:0] layer_out;\n' % (n-1)) # added
    f.write('always @(*) begin\n')
    f.write('\t' + 'case(mem_last_state)\n')
    f.write(2*'\t' + 'IDLE: layer_out = %d\'d0;\n' % (n))
    
    for st in range(sizes[current_layer]):
        f.write(2*'\t' + 'ST_%d: layer_out = ovfpos_trunc_acc_n%d ? ' 
                % (st+1, st))
        f.write('{%d{1\'b1}} : (ovfneg_trunc_acc_n%d ? {%d{1\'b0}} : mem_out);'
                % (n, st, n))
        f.write('\n')
    
    f.write(2*'\t' + 'DONE: layer_out = %d\'d0;\n' % (n))
    f.write(2*'\t' + 'default: layer_out = %d\'d0;\n' % (n))
    f.write('\t' + 'endcase\n')
    f.write('end' + 2*'\n')

    f.write('wire valid_out = (mem_state == ST_1);' + 2*'\n')
    
    f.write('always @(posedge clk or negedge rst_n) begin' + '\n')
    f.write('\t' + 'if (!rst_n) begin' + '\n')
    f.write(2*'\t' + 'layer_out_delayed <= 8\'d0;' + '\n')
    f.write(2*'\t' + 'delayed_valid_out <= 1\'d0;' + '\n')
    f.write('\t' + 'end' + '\n')
    f.write('\t' + 'else begin' + '\n')
    f.write(2*'\t' + 'layer_out_delayed <= layer_out;' + '\n')
    f.write(2*'\t' + 'delayed_valid_out <= valid_out;' + '\n')
    f.write('\t' + 'end' + '\n')
    f.write('end' + '\n')

    f.write('\n' + 'endmodule' + '\n')
    f.close()

    ###########################################################################
    ########################### HARDMAX GENERATION ############################
    ###########################################################################
    
module_name = 'hardmax'
module_filename = module_name + '.v'
module_path = '../modules/' + module_name + '/'
f = open(module_path + module_filename, 'w')

f.write('// File generated at ' + 
        datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '.' + 2*'\n')
f.write('module ' + module_name + '\n')
f.write('#(\n')
f.write('\t' + 'parameter integer DATA_LEN = 8' )
f.write('\n)\n')
f.write('(\n')
f.write('\t' + 'input clk,\n')
f.write('\t' + 'input rst_n,\n')
f.write('\t' + 'input start,\n')
f.write('\t' + 'input [DATA_LEN-1:0] data_in,\n')
f.write('\t' + 'output reg [%d:0] index,\n' % (out_bits-1))
f.write('\t' + 'output ready\n')
f.write(');\n\n')

for i in range (output_neurons):
    f.write('reg [DATA_LEN-1:0] reg_%d;\n' % i)

f.write('\n// FSM Parameters\n')
hardmax_fsm_bits = how_many_bits(output_neurons+2)
f.write('localparam SIZE = %d;\n' % hardmax_fsm_bits)
f.write('localparam IDLE = %d\'d0,\n' % hardmax_fsm_bits)
# states = output_neurons+1 plus IDLE and DONE
for i in range(1, output_neurons+2):
    f.write('\t' + 'ST_%d = %d\'d%d,\n' % (i, hardmax_fsm_bits, i))
f.write('\t' + 'DONE = %d\'d%d;\n' % (hardmax_fsm_bits, i+1))

f.write('\n// FSM Sequential Logic\n')
f.write('reg [SIZE-1:0] state, next_state;\n')
f.write('always@(posedge clk or negedge rst_n) begin\n')
f.write('\t' + 'if (!rst_n) state <= IDLE;\n')
f.write('\t' + 'else state <= next_state;\n')
f.write('end\n')

f.write('\n// FSM Next State Logic\n')
f.write('always@(*) begin\n')
f.write('\t' + 'case(state)\n')
f.write(2*'\t' + 'IDLE: begin\n')
f.write(3*'\t' + 'if (start) next_state <= ST_1;\n')
f.write(3*'\t' + 'else next_state <= IDLE;\n')
f.write(2*'\t' + 'end\n')
for i in range(1, output_neurons+1):
    f.write(2*'\t' + 'ST_%d: next_state <= ST_%d;\n' % (i, i+1))
f.write(2*'\t' + 'ST_%d: next_state <= DONE;\n' % (i+1))
f.write(2*'\t' + 'DONE: next_state <= IDLE;\n')
f.write(2*'\t' + 'default: next_state <= state;\n')

f.write('\t' + 'endcase\n')
f.write('end\n\n')

f.write('// FSM Output Logic\n')
#f.write('')
f.write('always@(posedge clk or negedge rst_n) begin\n')
f.write('\t' + 'if (!rst_n) begin\n')
for i in range(output_neurons):
    f.write(2*'\t' + 'reg_%d <= {(DATA_LEN){1\'b0}};\n' % i)
f.write('\t' + 'end\n')
f.write('\t' + 'else if (state == DONE) begin\n')
for i in range(output_neurons):
    f.write(2*'\t' + 'reg_%d <= {(DATA_LEN){1\'b0}};\n' % i)
f.write('\t' + 'end\n')
f.write('\t' + 'else begin\n')
f.write(2*'\t' + 'case(state)\n')
for i in range(output_neurons):
    f.write(3*'\t' + 'ST_%d:  reg_%d <= data_in;\n' % (i+1, i))
f.write(2*'\t' + 'endcase\n')
f.write('\t' + 'end\n')
f.write('end\n\n')


f.write('reg [DATA_LEN-1:0] max;\n')
f.write('always@(posedge clk or negedge rst_n) begin\n')
f.write('\t' + 'if (!rst_n) begin\n')
f.write(2*'\t' + 'index <= {(%d){1\'b0}};\n' % out_bits)
f.write(2*'\t' + 'max <= {(DATA_LEN){1\'b0}};\n')
f.write('\t' + 'end\n')
f.write('\t' + 'else if (state == DONE) begin\n')
f.write(2*'\t' + 'index <= {(%d){1\'b0}};\n' % out_bits)
f.write(2*'\t' + 'max <= {(DATA_LEN){1\'b0}};\n')
f.write('\t' + 'end\n')
f.write('\t' + 'else begin\n')
f.write(2*'\t' + 'case(state)\n')
for i in range (1, output_neurons):
    f.write(3*'\t' + 'ST_%d:  begin\n' % (i+2))
    if (i == 1):
        f.write(4*'\t' + 'if (reg_1 > reg_0) begin\n')
        f.write(5*'\t' + 'index <= %d\'d1;\n' % out_bits)
        f.write(5*'\t' + 'max <= reg_1;\n')
        f.write(4*'\t' + 'end\n')
        f.write(4*'\t' + 'else begin\n')
        f.write(5*'\t' + 'index <= %d\'d0;\n' % out_bits)
        f.write(5*'\t' + 'max <= reg_0;\n')
        f.write(4*'\t' + 'end\n')
    else:
        f.write(4* '\t' + 'if (reg_%d > max) begin\n' % i)
        f.write(5* '\t' + 'max <= reg_%d;\n' % i)
        f.write(5* '\t' + 'index <= %d\'d%d;\n' % (out_bits, i))
        f.write(4* '\t' + 'end\n')
    f.write(3*'\t' + 'end\n')
f.write(2*'\t' + 'endcase\n')
f.write('\t' + 'end\n')
f.write('end\n\n')

f.write('assign ready = (state == DONE);\n')

f.write('\n' + 'endmodule' + '\n')

f.close()


    ###########################################################################
    ########################### NETWORK GENERATION ############################
    ###########################################################################
    
module_name = 'network'
module_filename = module_name + '.v'
module_path = '../modules/' + module_name + '/'
f = open(module_path + module_filename, 'w')

f.write('// File generated at ' + 
        datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '.' + 2*'\n')
f.write('module ' + module_name + '\n')
f.write('#(\n')
f.write('\t' + 'parameter FRACT = %d,\n' % FRACT)
f.write('\t' + 'parameter WHOLE = %d,\n' % WHOLE)
f.write('\t' + 'parameter N = %d' % n)
f.write('\n)\n')
f.write('(\n')

f.write('\t' + 'input clk,\n')
f.write('\t' + 'input rst_n,\n')
f.write('\t' + 'input [N-1:0] net_in,\n')
f.write('\t' + 'input valid_in,\n')
f.write('\t' + 'output [%d:0] net_classification,\n' % (out_bits-1))
f.write('\t' + 'output net_valid_out\n')
f.write(');\n\n')


for i in range(1, len(sizes)):
    f.write('wire [N-1:0] out_l%d;\n' % i)
f.write('\n')

for i in range(1, len(sizes)):
    f.write('wire valid_out_l%d;\n' % i)
f.write('\n')
    
for i in range(1, len(sizes)):
    f.write('wire [%d:0] mem_index_l%d;\n' % (lut_addr_bits-1, i))
f.write('\n')


f.write('wire [%d:0] mem_index = ' % (lut_addr_bits-1))
for i in range(1, len(sizes)):
    f.write('mem_index_l%d' % i);
    if (i != (len(sizes)-1)):
        f.write(' | ');
    else:
        f.write(';\n\n')
        
f.write('wire [%d:0] mem_out;\n\n' % (n-1))


f.write('activationROM_%dx%dbits ' % (lut_entries, n))
f.write('activationROM_%dx%dbits_inst\n(' % (lut_entries, n))
f.write('\t' + '.clock (clk),' + '\n')
f.write('\t' + '.address (mem_index),' + '\n')
f.write('\t' + '.q (mem_out)' + '\n')
f.write(');' + '\n')

for current_layer in range(1, len(sizes)):
    f.write('layer%d #(' % (current_layer) + '\n')
    f.write('\t' + '.FRACT(FRACT),' + '\n')
    f.write('\t' + '.WHOLE(WHOLE)' + '\n')
    f.write(')' + '\n' + 'layer%d_inst' % (current_layer) + '\n' + '(' + '\n')
    
    f.write('\t' + '.clk(clk),' + '\n')
    f.write('\t' + '.rst_n(rst_n),' + '\n')
    if (current_layer == 1):
        f.write('\t' + '.layer_in(net_in),' + '\n')
        f.write('\t' + '.valid_in(valid_in),' + '\n')
    else:
        f.write('\t' + '.layer_in(out_l%d),' % (current_layer-1) + '\n')
        f.write('\t' + '.valid_in(valid_out_l%d),' % (current_layer-1) + '\n')
        
    f.write('\t' + '.mem_index(mem_index_l%d),\n' % current_layer)
    f.write('\t' + '.mem_out(mem_out),\n')
    
    f.write('\t' + '.layer_out_delayed(out_l%d),' % (current_layer) + '\n')
    f.write('\t' + '.delayed_valid_out(valid_out_l%d)' % (current_layer) + '\n')
    f.write(');' + 2*'\n')

f.write('hardmax' + '\n' + '#(' + '\n')
f.write('\t' + '.DATA_LEN(N)' + '\n')
f.write(')' + '\n')
f.write('hardmax_inst' + '\n' + '(' + '\n')
f.write('\t' + '.clk(clk),' + '\n')
f.write('\t' + '.rst_n(rst_n),' + '\n')
f.write('\t' + '.start(valid_out_l%d),' % (out_layer) + '\n')
f.write('\t' + '.data_in(out_l%d),' % (out_layer) + '\n')
f.write('\t' + '.index(net_classification),' + '\n')
f.write('\t' + '.ready(net_valid_out)' + '\n')
f.write(');' + 2*'\n')

f.write('\n' + 'endmodule' + '\n')

f.close()
