
# -*- coding: utf-8 -*-

from mnist_loader import *
training_data, validation_data, test_data = load_data()

# test_data[0] = imgs
# test_data[1] = labels
# pixel example: test_data[0][9999][783]


############### FILE WITH TEST SET PIXELS ###############

# Complete test set
f = open('../data/testset_pixels_10000.txt', 'w')
for i in range(10000):
    for j in range(len(test_data[0][i])): #784 
        f.write(str( int(test_data[0][i][j]*(2**8))) + '\n')
f.close()

# Reduced text file (500 images) for development purposes:
f = open('../data/testset_pixels_500.txt', 'w')

for i in range(500):
    for j in range(len(test_data[0][i])):
        f.write(str( int(test_data[0][i][j]*(2**8))) + '\n')

f.close()

# Reduced text file (50 images) for development purposes:
f = open('../data/testset_pixels_50.txt', 'w')

for i in range(50):
    for j in range(len(test_data[0][i])):
        f.write(str( int(test_data[0][i][j]*(2**8))) + '\n')

f.close()

# Reduced text file (25 images) for development purposes:
f = open('../data/testset_pixels_25.txt', 'w')

for i in range(25):
    for j in range(len(test_data[0][i])):
        f.write(str( int(test_data[0][i][j]*(2**8))) + '\n')

f.close()

# Reduced text file (10 images) for development purposes:
f = open('../data/testset_pixels_10.txt', 'w')

for i in range(10):
    for j in range(len(test_data[0][i])):
        f.write(str( int(test_data[0][i][j]*(2**8))) + '\n')

f.close()

# Reduced text file (1 image) for development purposes:
f = open('../data/testset_pixels_1.txt', 'w')

for i in range(1):
    for j in range(len(test_data[0][i])):
        f.write(str( int(1)) + '\n')

f.close()

#########################################################


############### FILE WITH TEST SET LABELS ###############

f = open('../data/testset_labels.txt', 'w')

for i in range(len(test_data[1])):
    f.write('{} {}'.format(i, test_data[1][i]) + '\n')
    
f.close()

#########################################################
