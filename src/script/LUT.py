# -*- coding: utf-8 -*-

n = 11 # choose the LUT resolution and run this script for the ROM init file.

###############################################################################
######################### IMPORTS AND HELPER FUNCTIONS ########################
###############################################################################

from math import *

def sigmoid(x):
    return 1/(1+e**-x)

import matplotlib.pyplot as plt

###############################################################################
################################# LUT DESIGN ##################################
###############################################################################
    
# This LUT design approach is described in:
# HIMAVATHI, S.; ANITHA, D.; MUTHURAMALINGAM, A. Feedforward neural network
# implementation in fpga using layer multiplexing for effective resource
# utilization. IEEE Transactions on Neural Networks, IEEE, v. 18, n. 3,
# P. 880-888, 2007.

# Determine the range of input (x) for which the range of output is between
# (2^-n) and (1-2^-n).
lower_limit = -log(((2**n)-1),e)
upper_limit =  log(((2**n)-1),e)
delta_x = log((0.5+(2**-n))/(0.5-(2**-n)),e)

def x_slice(delta_x):     # Improvement: we want the slice to be a power of 2
    i=1.0                 # so that the actual value of the weighted inputs
    while (i > delta_x):  # will be the LUT address.
        i = i/2
    return i

new_delta_x = x_slice(delta_x)
min_lut_entries = (upper_limit-lower_limit)/new_delta_x
lut_entries = int(2**ceil(log(min_lut_entries,2)))
m = log(lut_entries,2)
lut_addr_bits = int(log(lut_entries,2))
input_range = lut_entries*new_delta_x

# Computation of pre-LUT
x_array = []
y_array = []
for i in range(lut_entries):
    x = -input_range/2 + new_delta_x*i
    y = sigmoid(x)
    x_array.append(x)
    y_array.append(y)

# Reorder to consider negative x values inside LUT index
x_array_reordered = x_array[:]
y_array_reordered = y_array[:]
for i in range (int(lut_entries/2)):
    x_array_reordered.insert(0,x_array_reordered.pop())
    y_array_reordered.insert(0,y_array_reordered.pop())

#fig = plt.figure()
#plt.title('LUT organization')
#ax = fig.gca()
#ax.set_xlabel('x_axis')
#ax.set_ylabel('y_axis')
##ax.set_xticks(np.arange(-(input_range/2 + 1), (input_range/2 + 1), 1))
##ax.set_yticks(np.arange(0, 1.1, 0.1))
#plt.scatter(x_array, y_array_reordered)

# CREATE .mif FILE for ROM initialization
file_name = ('activationROM_%dx%dbits_init.mif' % (lut_entries, n))
f = open(file_name, 'w')
mif_header = 'WIDTH=%d;\nDEPTH=%d;\nADDRESS_RADIX = UNS;\nDATA_RADIX = UNS;\n\
CONTENT BEGIN\n' % (n, lut_entries)
f.write(mif_header)
for i in range (lut_entries):
    y_int = int(y_array_reordered[i]*(2**n))
    f.write('\t%d\t:\t%d;\n' % (i, y_int))
f.write('END;')
f.close()

# Design considerations
print('')
print(file_name)
print('input_range: %.1f' % input_range)

fraction_bits = log(1/new_delta_x,2)
integer_bits  = m-fraction_bits
print('For proper match absciss-index, consider %d bits for fraction and %d \
bits for integer part.' % (fraction_bits, integer_bits))
