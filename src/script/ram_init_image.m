clear all; clc;
A = imread('../data/lena_512x512.bmp');
B = imresize(A, [480 640]); % [NUMROWS NUMCOLS]
imwrite(B, '../data/lena_640x480.bmp')

% Code to create a .mif file

src = imread('../data/lena_640x480.bmp');
%gray = rgb2gray(src);
[m,n] = size(src); %size of your picture

N = m*n; %your ram or rom depth
word_len = 8; 
data = reshape(src', 1, N);% reshape you picture's data

fid=fopen('../data/lena_640x480.mif', 'w'); % open mif file 
fprintf(fid, 'WIDTH=%d;\n', word_len);
fprintf(fid, 'DEPTH=%d;\n', N);
fprintf(fid, 'ADDRESS_RADIX = UNS;\n'); 
fprintf(fid, 'DATA_RADIX = HEX;\n'); 
fprintf(fid, 'CONTENT\t');
fprintf(fid, 'BEGIN\n');

for i = 0 : N-1
fprintf(fid, '\t%d\t:\t%x;\n',i, data(i+1));
end

fprintf(fid, 'END;\n'); % prinf the end
fclose(fid); % close your file5
