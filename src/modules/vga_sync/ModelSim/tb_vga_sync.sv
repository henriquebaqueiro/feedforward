`timescale 1ns/100ps
module tb_vga_sync;

reg clk;
initial clk = 1'b0;
always #(1) clk = ~clk;

reg rst_n;
initial begin
  rst_n = 1;
  @(negedge clk);
  rst_n = 0;
  @(negedge clk);
  rst_n = 1;
end

wire vsync, hsync;
wire [9:0] hpos, vpos;
vga_sync vga_sync_inst (
  .clk(clk),
  .rst_n(rst_n),
  .hsync(hsync),
  .vsync(vsync),
  .hpos(hpos),
  .vpos(vpos)
);

initial begin
  repeat (3) @(negedge vsync);
  $stop;
end

endmodule
