//**********************************************************************//
//------------------- UNIVERSIDADE FEDERAL DA BAHIA --------------------//
//--------------------- Escola Politécnica da UFBa ---------------------//
//---------- Programa de Pós-Graduação em Engenharia Elétrica ----------//
//                                                                      //
// File name            : vga_sync.v                                    //
// File contents        : VGA Synchronizer                              //
//                       (Resolution: 640x480 @60Hz)                    //
// Design Engineer      : Henrique Baqueiro                             //
// Last Changed         : 12 April 2017 (version 0.2)                   //
//                                                                      //
//----------------------------------------------------------------------//
//**********************************************************************//

`timescale 1 ps / 1 ps

module vga_sync
#(
  parameter POS_LENGTH = 4'd10,

  parameter H_VISIBLE = 10'd640,  parameter V_VISIBLE = 10'd480,
  parameter H_FPORCH  = 10'd16,   parameter V_FPORCH  = 10'd10,
  parameter H_BPORCH  = 10'd48,   parameter V_BPORCH  = 10'd33,
  parameter H_SYNC    = 10'd96,   parameter V_SYNC    = 10'd2
)
(
  input  clk,                       // 25MHz (approx @60Hz)
  input  rst_n,
  
  output hsync,
  output vsync,
  
  output reg [POS_LENGTH-1:0] hpos, // horizontal position
  output reg [POS_LENGTH-1:0] vpos  // vertical   position
);

localparam [POS_LENGTH-1:0] HPOS_MAX = H_VISIBLE + H_FPORCH + H_BPORCH + H_SYNC;
localparam [POS_LENGTH-1:0] VPOS_MAX = V_VISIBLE + V_FPORCH + V_BPORCH + V_SYNC;

always @(posedge clk or negedge rst_n) begin
  if (!rst_n) begin
    hpos <= {POS_LENGTH{1'b0}};
    vpos <= {POS_LENGTH{1'b0}};
  end
  else begin
    if (hpos < HPOS_MAX) hpos <= hpos + 1'b1;
    else begin
      hpos <= {POS_LENGTH{1'b0}};
      if (vpos < VPOS_MAX)  vpos <= vpos + 1'b1;
      else                  vpos <= {POS_LENGTH{1'b0}};
    end
  end
end

assign hsync = (hpos > H_FPORCH) && (hpos < H_FPORCH + H_SYNC);
assign vsync = (vpos > V_FPORCH) && (vpos < V_FPORCH + V_SYNC);

endmodule
