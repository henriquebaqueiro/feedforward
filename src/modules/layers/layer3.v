// File generated at 2019-11-22 20:59:13.

module layer3
#(
	parameter FRACT = 13,
	parameter WHOLE = 2
)
(
	input clk,
	input rst_n,
	input [8:0] layer_in,
	input valid_in,
	output reg [10:0] mem_index,
	input [8:0] mem_out,
	output reg  [8:0] layer_out_delayed,
	output reg delayed_valid_out
);


wire signed [15:0] weight [0:9][0:29];
assign weight[0][0] = 16'b1_10_1000011000110; //-1.475888
assign weight[0][1] = 16'b1_11_0000100100111; //-0.964086
assign weight[0][2] = 16'b1_10_1101011010011; //-1.161839
assign weight[0][3] = 16'b1_10_0110011010001; //-1.599567
assign weight[0][4] = 16'b0_00_0010001010110; //0.135585
assign weight[0][5] = 16'b1_10_0010011111110; //-1.844091
assign weight[0][6] = 16'b0_00_0101010111010; //0.335312
assign weight[0][7] = 16'b0_01_1011100101101; //1.724271
assign weight[0][8] = 16'b0_01_1110011100001; //1.902528
assign weight[0][9] = 16'b1_01_1100100001101; //-2.217246
assign weight[0][10] = 16'b0_01_0111001110110; //1.452017
assign weight[0][11] = 16'b0_00_1111010100111; //0.957993
assign weight[0][12] = 16'b1_10_1010010101000; //-1.354601
assign weight[0][13] = 16'b0_00_0111001110011; //0.451554
assign weight[0][14] = 16'b1_11_0010001100111; //-0.862441
assign weight[0][15] = 16'b1_11_1010101000001; //-0.335890
assign weight[0][16] = 16'b1_10_0100011111010; //-1.719494
assign weight[0][17] = 16'b1_10_1100001001100; //-1.240783
assign weight[0][18] = 16'b1_10_1100111010100; //-1.192912
assign weight[0][19] = 16'b1_10_1110111010000; //-1.068456
assign weight[0][20] = 16'b1_10_0001011110001; //-1.908107
assign weight[0][21] = 16'b1_11_0010101110011; //-0.829804
assign weight[0][22] = 16'b1_10_0110001110100; //-1.610940
assign weight[0][23] = 16'b0_10_0010011011100; //2.151865
assign weight[0][24] = 16'b0_00_1110001010110; //0.885606
assign weight[0][25] = 16'b1_11_0110001101100; //-0.611892
assign weight[0][26] = 16'b0_01_1001100100111; //1.598627
assign weight[0][27] = 16'b0_01_1100100110010; //1.787361
assign weight[0][28] = 16'b1_10_0101101110001; //-1.642573
assign weight[0][29] = 16'b1_01_1000010000111; //-2.483546
assign weight[1][0] = 16'b0_01_0011111111111; //1.249995
assign weight[1][1] = 16'b0_01_0101111010011; //1.369535
assign weight[1][2] = 16'b0_00_0111100101010; //0.473908
assign weight[1][3] = 16'b1_10_0100010000100; //-1.733912
assign weight[1][4] = 16'b0_00_0110110111111; //0.429617
assign weight[1][5] = 16'b1_10_1101010110001; //-1.165909
assign weight[1][6] = 16'b1_10_0110111101011; //-1.565129
assign weight[1][7] = 16'b1_10_1100111000011; //-1.195058
assign weight[1][8] = 16'b1_10_0011100110000; //-1.775487
assign weight[1][9] = 16'b1_10_0100100110001; //-1.712800
assign weight[1][10] = 16'b1_10_0010101010100; //-1.833594
assign weight[1][11] = 16'b1_01_0100011010100; //-2.724152
assign weight[1][12] = 16'b0_01_0010111011101; //1.183279
assign weight[1][13] = 16'b1_00_1000000010000; //-3.498157
assign weight[1][14] = 16'b0_00_1101100001101; //0.845411
assign weight[1][15] = 16'b1_10_0010001011011; //-1.863932
assign weight[1][16] = 16'b0_00_1001000000000; //0.562530
assign weight[1][17] = 16'b0_01_1101111011100; //1.870633
assign weight[1][18] = 16'b0_01_0010011110100; //1.154879
assign weight[1][19] = 16'b1_10_0110011001000; //-1.600666
assign weight[1][20] = 16'b1_11_0110010001011; //-0.608057
assign weight[1][21] = 16'b0_00_1010001010001; //0.634956
assign weight[1][22] = 16'b0_00_1101000111101; //0.820043
assign weight[1][23] = 16'b1_11_0101011110011; //-0.657917
assign weight[1][24] = 16'b1_01_1110000000110; //-2.124314
assign weight[1][25] = 16'b0_10_1011101101000; //2.731477
assign weight[1][26] = 16'b1_10_0000110100110; //-1.948540
assign weight[1][27] = 16'b1_10_1000100000111; //-1.467960
assign weight[1][28] = 16'b0_01_0110011101011; //1.403688
assign weight[1][29] = 16'b0_00_1100111000100; //0.805246
assign weight[2][0] = 16'b1_00_1111111101010; //-3.002695
assign weight[2][1] = 16'b0_01_0100010001110; //1.267431
assign weight[2][2] = 16'b1_01_0101000000100; //-2.687111
assign weight[2][3] = 16'b0_00_1101100001010; //0.845088
assign weight[2][4] = 16'b0_00_0000101011101; //0.042658
assign weight[2][5] = 16'b1_10_0100100101001; //-1.713844
assign weight[2][6] = 16'b1_11_1011001011101; //-0.301239
assign weight[2][7] = 16'b0_01_0100011001101; //1.275045
assign weight[2][8] = 16'b0_00_0111000001100; //0.439030
assign weight[2][9] = 16'b0_00_1110110001110; //0.923702
assign weight[2][10] = 16'b1_01_1000001110110; //-2.485659
assign weight[2][11] = 16'b0_00_1011110000101; //0.735069
assign weight[2][12] = 16'b0_00_1001110000100; //0.609954
assign weight[2][13] = 16'b0_01_0110100111100; //1.413691
assign weight[2][14] = 16'b1_01_0111001111110; //-2.547240
assign weight[2][15] = 16'b0_01_0001010100010; //1.082370
assign weight[2][16] = 16'b1_01_0100001100000; //-2.738389
assign weight[2][17] = 16'b0_10_1011111110001; //2.748254
assign weight[2][18] = 16'b1_01_1011000100011; //-2.308334
assign weight[2][19] = 16'b1_11_0111001100100; //-0.550300
assign weight[2][20] = 16'b1_01_1100010111101; //-2.226957
assign weight[2][21] = 16'b1_10_1010111111011; //-1.313130
assign weight[2][22] = 16'b1_11_1100100100001; //-0.214754
assign weight[2][23] = 16'b1_10_1100001101101; //-1.236744
assign weight[2][24] = 16'b0_01_0100110010011; //1.299196
assign weight[2][25] = 16'b0_01_1110001001100; //1.884317
assign weight[2][26] = 16'b1_01_1001011000100; //-2.413644
assign weight[2][27] = 16'b1_01_1100010010101; //-2.231814
assign weight[2][28] = 16'b0_00_1010100100111; //0.661024
assign weight[2][29] = 16'b0_01_0001010100110; //1.082819
assign weight[3][0] = 16'b0_01_0110000001001; //1.376124
assign weight[3][1] = 16'b1_00_1111010000011; //-3.046601
assign weight[3][2] = 16'b0_10_0110000000010; //2.375352
assign weight[3][3] = 16'b1_11_1010100010100; //-0.341376
assign weight[3][4] = 16'b1_11_1000100110001; //-0.462851
assign weight[3][5] = 16'b0_00_0101001011110; //0.323996
assign weight[3][6] = 16'b1_10_0010011110110; //-1.845044
assign weight[3][7] = 16'b0_00_0010111011000; //0.182675
assign weight[3][8] = 16'b1_10_0001100011101; //-1.902807
assign weight[3][9] = 16'b1_11_0010101011011; //-0.832653
assign weight[3][10] = 16'b0_01_1000110010011; //1.549235
assign weight[3][11] = 16'b0_00_0010011100101; //0.152997
assign weight[3][12] = 16'b1_01_0001000110011; //-2.931371
assign weight[3][13] = 16'b0_00_1111111011000; //0.995152
assign weight[3][14] = 16'b1_00_0111011010011; //-3.536762
assign weight[3][15] = 16'b1_11_0001010000110; //-0.921256
assign weight[3][16] = 16'b1_01_0110111000111; //-2.569487
assign weight[3][17] = 16'b0_01_1000100010111; //1.534105
assign weight[3][18] = 16'b0_00_1101110111011; //0.866665
assign weight[3][19] = 16'b1_10_1001110011011; //-1.387371
assign weight[3][20] = 16'b0_01_0111100001001; //1.469892
assign weight[3][21] = 16'b1_11_0100111111010; //-0.688298
assign weight[3][22] = 16'b1_11_1010110100001; //-0.324198
assign weight[3][23] = 16'b0_01_0100010000000; //1.265637
assign weight[3][24] = 16'b0_01_0101001000110; //1.321164
assign weight[3][25] = 16'b1_00_1111001011100; //-3.051376
assign weight[3][26] = 16'b1_10_1111011011000; //-1.036199
assign weight[3][27] = 16'b1_01_0000100011010; //-2.965620
assign weight[3][28] = 16'b0_10_0110100110100; //2.412636
assign weight[3][29] = 16'b0_01_1101001001111; //1.822198
assign weight[4][0] = 16'b1_11_0111110101110; //-0.510088
assign weight[4][1] = 16'b0_00_1001011101010; //0.591073
assign weight[4][2] = 16'b0_01_0100111111000; //1.311602
assign weight[4][3] = 16'b0_01_0111011010010; //1.463140
assign weight[4][4] = 16'b0_01_0111011101000; //1.465864
assign weight[4][5] = 16'b0_01_1101011101001; //1.840962
assign weight[4][6] = 16'b1_11_1100110010100; //-0.200701
assign weight[4][7] = 16'b1_10_1111000100110; //-1.057930
assign weight[4][8] = 16'b1_11_0111011101011; //-0.533899
assign weight[4][9] = 16'b1_11_0001100110110; //-0.899764
assign weight[4][10] = 16'b1_01_1110010101001; //-2.104403
assign weight[4][11] = 16'b1_00_1100010101001; //-3.229482
assign weight[4][12] = 16'b1_01_1100111101101; //-2.189911
assign weight[4][13] = 16'b0_00_1100000011010; //0.753239
assign weight[4][14] = 16'b0_00_0000100100001; //0.035332
assign weight[4][15] = 16'b0_10_0010011110010; //2.154580
assign weight[4][16] = 16'b0_00_0101111001101; //0.368781
assign weight[4][17] = 16'b0_00_0101010100000; //0.332092
assign weight[4][18] = 16'b1_10_0011111101010; //-1.752708
assign weight[4][19] = 16'b0_10_0100011100000; //2.277389
assign weight[4][20] = 16'b1_00_1010001010001; //-3.365131
assign weight[4][21] = 16'b1_01_1001000001111; //-2.435682
assign weight[4][22] = 16'b1_10_1001110100100; //-1.386251
assign weight[4][23] = 16'b1_11_0001111010111; //-0.880092
assign weight[4][24] = 16'b1_01_0110100001100; //-2.592336
assign weight[4][25] = 16'b1_10_1000111101000; //-1.440518
assign weight[4][26] = 16'b0_10_0111001011000; //2.448271
assign weight[4][27] = 16'b0_00_1001100101111; //0.599531
assign weight[4][28] = 16'b0_01_0111111100010; //1.496341
assign weight[4][29] = 16'b1_01_1010001000111; //-2.366390
assign weight[5][0] = 16'b0_00_1000110101111; //0.552644
assign weight[5][1] = 16'b1_01_1001110110110; //-2.384113
assign weight[5][2] = 16'b1_01_0000011100111; //-2.971862
assign weight[5][3] = 16'b1_10_0100010100001; //-1.730464
assign weight[5][4] = 16'b1_10_0111101011110; //-1.519888
assign weight[5][5] = 16'b0_01_1000011100001; //1.527470
assign weight[5][6] = 16'b0_00_0001100000011; //0.094207
assign weight[5][7] = 16'b1_10_0011100100000; //-1.777400
assign weight[5][8] = 16'b1_01_0110011010001; //-2.599602
assign weight[5][9] = 16'b0_10_0111000111101; //2.445045
assign weight[5][10] = 16'b1_01_1111011110100; //-2.032792
assign weight[5][11] = 16'b1_10_0011111101001; //-1.752919
assign weight[5][12] = 16'b1_01_1110100101000; //-2.088885
assign weight[5][13] = 16'b1_10_1010001001100; //-1.365839
assign weight[5][14] = 16'b0_10_0001000100010; //2.066695
assign weight[5][15] = 16'b1_11_0000011010011; //-0.974244
assign weight[5][16] = 16'b1_11_0111111001011; //-0.506520
assign weight[5][17] = 16'b1_01_1011101000011; //-2.273121
assign weight[5][18] = 16'b0_01_0111101011011; //1.479893
assign weight[5][19] = 16'b1_10_0010000101011; //-1.869815
assign weight[5][20] = 16'b1_01_0111110110010; //-2.509570
assign weight[5][21] = 16'b1_10_1111111001000; //-1.006854
assign weight[5][22] = 16'b0_01_1110101111101; //1.921628
assign weight[5][23] = 16'b1_10_0111011100101; //-1.534617
assign weight[5][24] = 16'b0_01_1001010100010; //1.582287
assign weight[5][25] = 16'b1_11_0100001000110; //-0.741525
assign weight[5][26] = 16'b0_01_1101101111001; //1.858592
assign weight[5][27] = 16'b0_00_0110110100000; //0.425886
assign weight[5][28] = 16'b1_01_1010010011001; //-2.356436
assign weight[5][29] = 16'b0_00_0101110110110; //0.366026
assign weight[6][0] = 16'b1_11_1011111011111; //-0.254113
assign weight[6][1] = 16'b0_00_0110011101011; //0.403797
assign weight[6][2] = 16'b1_10_0101110101101; //-1.635155
assign weight[6][3] = 16'b0_00_0110101100000; //0.417984
assign weight[6][4] = 16'b1_10_1010111111010; //-1.313267
assign weight[6][5] = 16'b1_11_0111011011010; //-0.535954
assign weight[6][6] = 16'b1_10_1011101011001; //-1.270477
assign weight[6][7] = 16'b0_00_0110101000001; //0.414289
assign weight[6][8] = 16'b1_10_0010001111101; //-1.859745
assign weight[6][9] = 16'b1_01_1101110001001; //-2.139533
assign weight[6][10] = 16'b1_10_1011111101111; //-1.252152
assign weight[6][11] = 16'b0_01_0000101101100; //1.044502
assign weight[6][12] = 16'b0_10_1101100110111; //2.850506
assign weight[6][13] = 16'b1_01_0001101111100; //-2.891147
assign weight[6][14] = 16'b0_01_0100100000011; //1.281641
assign weight[6][15] = 16'b0_01_0101010110001; //1.334196
assign weight[6][16] = 16'b0_01_1110001100111; //1.887620
assign weight[6][17] = 16'b1_01_0100110011100; //-2.699769
assign weight[6][18] = 16'b1_01_0110011010001; //-2.599524
assign weight[6][19] = 16'b1_10_1001010101101; //-1.416464
assign weight[6][20] = 16'b1_11_0010111010011; //-0.818038
assign weight[6][21] = 16'b0_01_1001110010110; //1.612156
assign weight[6][22] = 16'b1_00_1100001110100; //-3.235954
assign weight[6][23] = 16'b1_11_1001100100100; //-0.401941
assign weight[6][24] = 16'b0_01_0000101100001; //1.043177
assign weight[6][25] = 16'b1_10_0111001010000; //-1.552833
assign weight[6][26] = 16'b0_01_1001111000100; //1.617731
assign weight[6][27] = 16'b1_11_0101110001000; //-0.639676
assign weight[6][28] = 16'b0_01_0011010011000; //1.206121
assign weight[6][29] = 16'b0_00_1110000000100; //0.875506
assign weight[7][0] = 16'b1_01_1101001101001; //-2.174696
assign weight[7][1] = 16'b0_00_0001111110111; //0.124000
assign weight[7][2] = 16'b1_10_0011110101001; //-1.760687
assign weight[7][3] = 16'b1_10_0110100100000; //-1.589872
assign weight[7][4] = 16'b0_00_0010011010110; //0.151200
assign weight[7][5] = 16'b0_01_1100100001011; //1.782620
assign weight[7][6] = 16'b0_01_0001000000100; //1.063108
assign weight[7][7] = 16'b1_10_1011100111111; //-1.273649
assign weight[7][8] = 16'b1_11_1111001110100; //-0.048403
assign weight[7][9] = 16'b1_11_1111111101101; //-0.002372
assign weight[7][10] = 16'b0_01_0111000010101; //1.440156
assign weight[7][11] = 16'b1_01_1111000011000; //-2.059597
assign weight[7][12] = 16'b1_10_1100011101001; //-1.221585
assign weight[7][13] = 16'b0_00_0110001111010; //0.389966
assign weight[7][14] = 16'b1_11_0010001010110; //-0.864559
assign weight[7][15] = 16'b1_01_1011100010001; //-2.279291
assign weight[7][16] = 16'b1_01_1101010010010; //-2.169681
assign weight[7][17] = 16'b0_01_0111111010000; //1.494192
assign weight[7][18] = 16'b1_11_1010111101000; //-0.315469
assign weight[7][19] = 16'b0_10_0010110000110; //2.172717
assign weight[7][20] = 16'b0_01_0010100100010; //1.160484
assign weight[7][21] = 16'b0_10_1000011110001; //2.529500
assign weight[7][22] = 16'b0_00_0100010101010; //0.270857
assign weight[7][23] = 16'b1_11_0010011110011; //-0.845422
assign weight[7][24] = 16'b1_01_0111001010110; //-2.552023
assign weight[7][25] = 16'b0_11_0011010111011; //3.210366
assign weight[7][26] = 16'b1_11_1100101110000; //-0.205108
assign weight[7][27] = 16'b1_10_1101010001111; //-1.170108
assign weight[7][28] = 16'b1_10_1110010010010; //-1.107247
assign weight[7][29] = 16'b1_01_1001110001101; //-2.389053
assign weight[8][0] = 16'b0_01_0011011110110; //1.217616
assign weight[8][1] = 16'b1_01_1000110101001; //-2.448199
assign weight[8][2] = 16'b1_10_1101011010111; //-1.161373
assign weight[8][3] = 16'b1_11_0010011010111; //-0.848819
assign weight[8][4] = 16'b0_01_0011100001111; //1.220594
assign weight[8][5] = 16'b1_01_1101000111101; //-2.180128
assign weight[8][6] = 16'b0_00_0100000111100; //0.257358
assign weight[8][7] = 16'b1_10_0111100110101; //-1.524797
assign weight[8][8] = 16'b0_01_0111111100001; //1.496257
assign weight[8][9] = 16'b0_00_0000100110111; //0.038085
assign weight[8][10] = 16'b1_11_0100011001011; //-0.725335
assign weight[8][11] = 16'b0_01_0011110010001; //1.236534
assign weight[8][12] = 16'b0_01_1111001111000; //1.952248
assign weight[8][13] = 16'b0_00_0110110101110; //0.427492
assign weight[8][14] = 16'b1_00_0011011000001; //-3.789025
assign weight[8][15] = 16'b1_10_1100111010110; //-1.192701
assign weight[8][16] = 16'b0_01_0010011101100; //1.153884
assign weight[8][17] = 16'b1_01_0001111000101; //-2.882262
assign weight[8][18] = 16'b1_10_1000110010101; //-1.450639
assign weight[8][19] = 16'b1_01_1011110001100; //-2.264194
assign weight[8][20] = 16'b1_01_1101111111000; //-2.126080
assign weight[8][21] = 16'b1_10_1011110010001; //-1.263563
assign weight[8][22] = 16'b0_01_1011010101111; //1.708975
assign weight[8][23] = 16'b1_01_1100011110010; //-2.220520
assign weight[8][24] = 16'b1_00_1111100001111; //-3.029491
assign weight[8][25] = 16'b1_01_0010101110011; //-2.829728
assign weight[8][26] = 16'b1_10_0110001011110; //-1.613625
assign weight[8][27] = 16'b0_01_0001101101110; //1.107243
assign weight[8][28] = 16'b0_01_1010010001100; //1.642201
assign weight[8][29] = 16'b0_00_0101001110011; //0.326610
assign weight[9][0] = 16'b0_00_0100111010100; //0.307207
assign weight[9][1] = 16'b1_10_1001000101011; //-1.432297
assign weight[9][2] = 16'b0_00_1011000001011; //0.688938
assign weight[9][3] = 16'b0_10_1000101100010; //2.543272
assign weight[9][4] = 16'b1_11_0000000101010; //-0.994875
assign weight[9][5] = 16'b1_00_0100110111110; //-3.695575
assign weight[9][6] = 16'b1_10_0100001010010; //-1.740003
assign weight[9][7] = 16'b1_11_1110000110110; //-0.118436
assign weight[9][8] = 16'b1_11_0111011101000; //-0.534180
assign weight[9][9] = 16'b0_00_0000101011000; //0.042110
assign weight[9][10] = 16'b1_11_0011101111101; //-0.766010
assign weight[9][11] = 16'b0_01_1010000010000; //1.627043
assign weight[9][12] = 16'b1_10_0111101100110; //-1.518892
assign weight[9][13] = 16'b1_11_1101010110111; //-0.165248
assign weight[9][14] = 16'b0_01_1011111010110; //1.744985
assign weight[9][15] = 16'b1_01_1000010001001; //-2.483350
assign weight[9][16] = 16'b0_01_0010111110000; //1.185658
assign weight[9][17] = 16'b1_01_1011111011110; //-2.254163
assign weight[9][18] = 16'b0_01_0011010010000; //1.205162
assign weight[9][19] = 16'b0_10_1001000001111; //2.564418
assign weight[9][20] = 16'b0_10_0101011111000; //2.342895
assign weight[9][21] = 16'b1_01_1110001010101; //-2.114647
assign weight[9][22] = 16'b1_10_1111101010110; //-1.020772
assign weight[9][23] = 16'b1_11_1000010010001; //-0.482401
assign weight[9][24] = 16'b1_11_0010010010010; //-0.857262
assign weight[9][25] = 16'b1_01_1000100010011; //-2.466440
assign weight[9][26] = 16'b1_01_0001110011101; //-2.887152
assign weight[9][27] = 16'b1_11_0101100100001; //-0.652277
assign weight[9][28] = 16'b1_01_0000111110101; //-2.938896
assign weight[9][29] = 16'b1_00_1111100111010; //-3.024221

wire signed [15:0] bias [0:9];
assign bias[0] = 16'b1_10_1100101011100; //-1.207563
assign bias[1] = 16'b1_01_1111111101110; //-2.002210
assign bias[2] = 16'b1_00_1011011011101; //-3.285598
assign bias[3] = 16'b1_01_0110101111110; //-2.578412
assign bias[4] = 16'b1_00_1010010110111; //-3.352669
assign bias[5] = 16'b1_11_1100000011100; //-0.246612
assign bias[6] = 16'b1_00_0010111101001; //-3.815416
assign bias[7] = 16'b1_01_0011010010000; //-2.794996
assign bias[8] = 16'b1_11_1000101100111; //-0.456221
assign bias[9] = 16'b0_00_0101100110100; //0.350162


wire signed [(1+WHOLE+FRACT)-1:0] layer_in_ext = {1'b0, {WHOLE{1'b0}}, layer_in, {(FRACT-9){1'b0}}};

// FSM FOR LAYER STATE
localparam IDLE	= 5'd0,
          ST_1= 5'd1,
          ST_2= 5'd2,
          ST_3= 5'd3,
          ST_4= 5'd4,
          ST_5= 5'd5,
          ST_6= 5'd6,
          ST_7= 5'd7,
          ST_8= 5'd8,
          ST_9= 5'd9,
          ST_10= 5'd10,
          ST_11= 5'd11,
          ST_12= 5'd12,
          ST_13= 5'd13,
          ST_14= 5'd14,
          ST_15= 5'd15,
          ST_16= 5'd16,
          ST_17= 5'd17,
          ST_18= 5'd18,
          ST_19= 5'd19,
          ST_20= 5'd20,
          ST_21= 5'd21,
          ST_22= 5'd22,
          ST_23= 5'd23,
          ST_24= 5'd24,
          ST_25= 5'd25,
          ST_26= 5'd26,
          ST_27= 5'd27,
          ST_28= 5'd28,
          ST_29= 5'd29,
          ST_30= 5'd30,
          DONE  = 5'd31;

reg [4:0] state, next_state;
always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) state <= IDLE;
    else        state <= next_state;
end

always @(*) begin
    case(state)
        IDLE: if (valid_in) next_state = ST_1;
              else          next_state = IDLE;
        ST_1: next_state = ST_2;
        ST_2: next_state = ST_3;
        ST_3: next_state = ST_4;
        ST_4: next_state = ST_5;
        ST_5: next_state = ST_6;
        ST_6: next_state = ST_7;
        ST_7: next_state = ST_8;
        ST_8: next_state = ST_9;
        ST_9: next_state = ST_10;
        ST_10: next_state = ST_11;
        ST_11: next_state = ST_12;
        ST_12: next_state = ST_13;
        ST_13: next_state = ST_14;
        ST_14: next_state = ST_15;
        ST_15: next_state = ST_16;
        ST_16: next_state = ST_17;
        ST_17: next_state = ST_18;
        ST_18: next_state = ST_19;
        ST_19: next_state = ST_20;
        ST_20: next_state = ST_21;
        ST_21: next_state = ST_22;
        ST_22: next_state = ST_23;
        ST_23: next_state = ST_24;
        ST_24: next_state = ST_25;
        ST_25: next_state = ST_26;
        ST_26: next_state = ST_27;
        ST_27: next_state = ST_28;
        ST_28: next_state = ST_29;
        ST_29: next_state = ST_30;
        ST_30: next_state = DONE;
        DONE: next_state = IDLE;
        default: next_state = IDLE;
    endcase
end

// WEIGHT MULTIPLEXING
reg acc_en;
reg signed [(1+WHOLE+FRACT)-1:0] weight_n0;
reg signed [(1+WHOLE+FRACT)-1:0] weight_n1;
reg signed [(1+WHOLE+FRACT)-1:0] weight_n2;
reg signed [(1+WHOLE+FRACT)-1:0] weight_n3;
reg signed [(1+WHOLE+FRACT)-1:0] weight_n4;
reg signed [(1+WHOLE+FRACT)-1:0] weight_n5;
reg signed [(1+WHOLE+FRACT)-1:0] weight_n6;
reg signed [(1+WHOLE+FRACT)-1:0] weight_n7;
reg signed [(1+WHOLE+FRACT)-1:0] weight_n8;
reg signed [(1+WHOLE+FRACT)-1:0] weight_n9;

always @(*) begin
    case(state)

        IDLE: begin
           acc_en = 1'b0;
           weight_n0 = 16'd0;
           weight_n1 = 16'd0;
           weight_n2 = 16'd0;
           weight_n3 = 16'd0;
           weight_n4 = 16'd0;
           weight_n5 = 16'd0;
           weight_n6 = 16'd0;
           weight_n7 = 16'd0;
           weight_n8 = 16'd0;
           weight_n9 = 16'd0;
        end

        ST_1: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][0];
           weight_n1 = weight[1][0];
           weight_n2 = weight[2][0];
           weight_n3 = weight[3][0];
           weight_n4 = weight[4][0];
           weight_n5 = weight[5][0];
           weight_n6 = weight[6][0];
           weight_n7 = weight[7][0];
           weight_n8 = weight[8][0];
           weight_n9 = weight[9][0];
        end

        ST_2: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][1];
           weight_n1 = weight[1][1];
           weight_n2 = weight[2][1];
           weight_n3 = weight[3][1];
           weight_n4 = weight[4][1];
           weight_n5 = weight[5][1];
           weight_n6 = weight[6][1];
           weight_n7 = weight[7][1];
           weight_n8 = weight[8][1];
           weight_n9 = weight[9][1];
        end

        ST_3: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][2];
           weight_n1 = weight[1][2];
           weight_n2 = weight[2][2];
           weight_n3 = weight[3][2];
           weight_n4 = weight[4][2];
           weight_n5 = weight[5][2];
           weight_n6 = weight[6][2];
           weight_n7 = weight[7][2];
           weight_n8 = weight[8][2];
           weight_n9 = weight[9][2];
        end

        ST_4: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][3];
           weight_n1 = weight[1][3];
           weight_n2 = weight[2][3];
           weight_n3 = weight[3][3];
           weight_n4 = weight[4][3];
           weight_n5 = weight[5][3];
           weight_n6 = weight[6][3];
           weight_n7 = weight[7][3];
           weight_n8 = weight[8][3];
           weight_n9 = weight[9][3];
        end

        ST_5: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][4];
           weight_n1 = weight[1][4];
           weight_n2 = weight[2][4];
           weight_n3 = weight[3][4];
           weight_n4 = weight[4][4];
           weight_n5 = weight[5][4];
           weight_n6 = weight[6][4];
           weight_n7 = weight[7][4];
           weight_n8 = weight[8][4];
           weight_n9 = weight[9][4];
        end

        ST_6: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][5];
           weight_n1 = weight[1][5];
           weight_n2 = weight[2][5];
           weight_n3 = weight[3][5];
           weight_n4 = weight[4][5];
           weight_n5 = weight[5][5];
           weight_n6 = weight[6][5];
           weight_n7 = weight[7][5];
           weight_n8 = weight[8][5];
           weight_n9 = weight[9][5];
        end

        ST_7: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][6];
           weight_n1 = weight[1][6];
           weight_n2 = weight[2][6];
           weight_n3 = weight[3][6];
           weight_n4 = weight[4][6];
           weight_n5 = weight[5][6];
           weight_n6 = weight[6][6];
           weight_n7 = weight[7][6];
           weight_n8 = weight[8][6];
           weight_n9 = weight[9][6];
        end

        ST_8: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][7];
           weight_n1 = weight[1][7];
           weight_n2 = weight[2][7];
           weight_n3 = weight[3][7];
           weight_n4 = weight[4][7];
           weight_n5 = weight[5][7];
           weight_n6 = weight[6][7];
           weight_n7 = weight[7][7];
           weight_n8 = weight[8][7];
           weight_n9 = weight[9][7];
        end

        ST_9: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][8];
           weight_n1 = weight[1][8];
           weight_n2 = weight[2][8];
           weight_n3 = weight[3][8];
           weight_n4 = weight[4][8];
           weight_n5 = weight[5][8];
           weight_n6 = weight[6][8];
           weight_n7 = weight[7][8];
           weight_n8 = weight[8][8];
           weight_n9 = weight[9][8];
        end

        ST_10: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][9];
           weight_n1 = weight[1][9];
           weight_n2 = weight[2][9];
           weight_n3 = weight[3][9];
           weight_n4 = weight[4][9];
           weight_n5 = weight[5][9];
           weight_n6 = weight[6][9];
           weight_n7 = weight[7][9];
           weight_n8 = weight[8][9];
           weight_n9 = weight[9][9];
        end

        ST_11: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][10];
           weight_n1 = weight[1][10];
           weight_n2 = weight[2][10];
           weight_n3 = weight[3][10];
           weight_n4 = weight[4][10];
           weight_n5 = weight[5][10];
           weight_n6 = weight[6][10];
           weight_n7 = weight[7][10];
           weight_n8 = weight[8][10];
           weight_n9 = weight[9][10];
        end

        ST_12: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][11];
           weight_n1 = weight[1][11];
           weight_n2 = weight[2][11];
           weight_n3 = weight[3][11];
           weight_n4 = weight[4][11];
           weight_n5 = weight[5][11];
           weight_n6 = weight[6][11];
           weight_n7 = weight[7][11];
           weight_n8 = weight[8][11];
           weight_n9 = weight[9][11];
        end

        ST_13: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][12];
           weight_n1 = weight[1][12];
           weight_n2 = weight[2][12];
           weight_n3 = weight[3][12];
           weight_n4 = weight[4][12];
           weight_n5 = weight[5][12];
           weight_n6 = weight[6][12];
           weight_n7 = weight[7][12];
           weight_n8 = weight[8][12];
           weight_n9 = weight[9][12];
        end

        ST_14: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][13];
           weight_n1 = weight[1][13];
           weight_n2 = weight[2][13];
           weight_n3 = weight[3][13];
           weight_n4 = weight[4][13];
           weight_n5 = weight[5][13];
           weight_n6 = weight[6][13];
           weight_n7 = weight[7][13];
           weight_n8 = weight[8][13];
           weight_n9 = weight[9][13];
        end

        ST_15: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][14];
           weight_n1 = weight[1][14];
           weight_n2 = weight[2][14];
           weight_n3 = weight[3][14];
           weight_n4 = weight[4][14];
           weight_n5 = weight[5][14];
           weight_n6 = weight[6][14];
           weight_n7 = weight[7][14];
           weight_n8 = weight[8][14];
           weight_n9 = weight[9][14];
        end

        ST_16: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][15];
           weight_n1 = weight[1][15];
           weight_n2 = weight[2][15];
           weight_n3 = weight[3][15];
           weight_n4 = weight[4][15];
           weight_n5 = weight[5][15];
           weight_n6 = weight[6][15];
           weight_n7 = weight[7][15];
           weight_n8 = weight[8][15];
           weight_n9 = weight[9][15];
        end

        ST_17: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][16];
           weight_n1 = weight[1][16];
           weight_n2 = weight[2][16];
           weight_n3 = weight[3][16];
           weight_n4 = weight[4][16];
           weight_n5 = weight[5][16];
           weight_n6 = weight[6][16];
           weight_n7 = weight[7][16];
           weight_n8 = weight[8][16];
           weight_n9 = weight[9][16];
        end

        ST_18: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][17];
           weight_n1 = weight[1][17];
           weight_n2 = weight[2][17];
           weight_n3 = weight[3][17];
           weight_n4 = weight[4][17];
           weight_n5 = weight[5][17];
           weight_n6 = weight[6][17];
           weight_n7 = weight[7][17];
           weight_n8 = weight[8][17];
           weight_n9 = weight[9][17];
        end

        ST_19: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][18];
           weight_n1 = weight[1][18];
           weight_n2 = weight[2][18];
           weight_n3 = weight[3][18];
           weight_n4 = weight[4][18];
           weight_n5 = weight[5][18];
           weight_n6 = weight[6][18];
           weight_n7 = weight[7][18];
           weight_n8 = weight[8][18];
           weight_n9 = weight[9][18];
        end

        ST_20: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][19];
           weight_n1 = weight[1][19];
           weight_n2 = weight[2][19];
           weight_n3 = weight[3][19];
           weight_n4 = weight[4][19];
           weight_n5 = weight[5][19];
           weight_n6 = weight[6][19];
           weight_n7 = weight[7][19];
           weight_n8 = weight[8][19];
           weight_n9 = weight[9][19];
        end

        ST_21: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][20];
           weight_n1 = weight[1][20];
           weight_n2 = weight[2][20];
           weight_n3 = weight[3][20];
           weight_n4 = weight[4][20];
           weight_n5 = weight[5][20];
           weight_n6 = weight[6][20];
           weight_n7 = weight[7][20];
           weight_n8 = weight[8][20];
           weight_n9 = weight[9][20];
        end

        ST_22: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][21];
           weight_n1 = weight[1][21];
           weight_n2 = weight[2][21];
           weight_n3 = weight[3][21];
           weight_n4 = weight[4][21];
           weight_n5 = weight[5][21];
           weight_n6 = weight[6][21];
           weight_n7 = weight[7][21];
           weight_n8 = weight[8][21];
           weight_n9 = weight[9][21];
        end

        ST_23: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][22];
           weight_n1 = weight[1][22];
           weight_n2 = weight[2][22];
           weight_n3 = weight[3][22];
           weight_n4 = weight[4][22];
           weight_n5 = weight[5][22];
           weight_n6 = weight[6][22];
           weight_n7 = weight[7][22];
           weight_n8 = weight[8][22];
           weight_n9 = weight[9][22];
        end

        ST_24: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][23];
           weight_n1 = weight[1][23];
           weight_n2 = weight[2][23];
           weight_n3 = weight[3][23];
           weight_n4 = weight[4][23];
           weight_n5 = weight[5][23];
           weight_n6 = weight[6][23];
           weight_n7 = weight[7][23];
           weight_n8 = weight[8][23];
           weight_n9 = weight[9][23];
        end

        ST_25: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][24];
           weight_n1 = weight[1][24];
           weight_n2 = weight[2][24];
           weight_n3 = weight[3][24];
           weight_n4 = weight[4][24];
           weight_n5 = weight[5][24];
           weight_n6 = weight[6][24];
           weight_n7 = weight[7][24];
           weight_n8 = weight[8][24];
           weight_n9 = weight[9][24];
        end

        ST_26: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][25];
           weight_n1 = weight[1][25];
           weight_n2 = weight[2][25];
           weight_n3 = weight[3][25];
           weight_n4 = weight[4][25];
           weight_n5 = weight[5][25];
           weight_n6 = weight[6][25];
           weight_n7 = weight[7][25];
           weight_n8 = weight[8][25];
           weight_n9 = weight[9][25];
        end

        ST_27: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][26];
           weight_n1 = weight[1][26];
           weight_n2 = weight[2][26];
           weight_n3 = weight[3][26];
           weight_n4 = weight[4][26];
           weight_n5 = weight[5][26];
           weight_n6 = weight[6][26];
           weight_n7 = weight[7][26];
           weight_n8 = weight[8][26];
           weight_n9 = weight[9][26];
        end

        ST_28: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][27];
           weight_n1 = weight[1][27];
           weight_n2 = weight[2][27];
           weight_n3 = weight[3][27];
           weight_n4 = weight[4][27];
           weight_n5 = weight[5][27];
           weight_n6 = weight[6][27];
           weight_n7 = weight[7][27];
           weight_n8 = weight[8][27];
           weight_n9 = weight[9][27];
        end

        ST_29: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][28];
           weight_n1 = weight[1][28];
           weight_n2 = weight[2][28];
           weight_n3 = weight[3][28];
           weight_n4 = weight[4][28];
           weight_n5 = weight[5][28];
           weight_n6 = weight[6][28];
           weight_n7 = weight[7][28];
           weight_n8 = weight[8][28];
           weight_n9 = weight[9][28];
        end

        ST_30: begin
           acc_en = 1'b1;
           weight_n0 = weight[0][29];
           weight_n1 = weight[1][29];
           weight_n2 = weight[2][29];
           weight_n3 = weight[3][29];
           weight_n4 = weight[4][29];
           weight_n5 = weight[5][29];
           weight_n6 = weight[6][29];
           weight_n7 = weight[7][29];
           weight_n8 = weight[8][29];
           weight_n9 = weight[9][29];
        end

        DONE: begin
           acc_en = 1'b0;
           weight_n0 = 16'd0;
           weight_n1 = 16'd0;
           weight_n2 = 16'd0;
           weight_n3 = 16'd0;
           weight_n4 = 16'd0;
           weight_n5 = 16'd0;
           weight_n6 = 16'd0;
           weight_n7 = 16'd0;
           weight_n8 = 16'd0;
           weight_n9 = 16'd0;
        end

        default: begin
           acc_en = 1'b0;
           weight_n0 = 16'd0;
           weight_n1 = 16'd0;
           weight_n2 = 16'd0;
           weight_n3 = 16'd0;
           weight_n4 = 16'd0;
           weight_n5 = 16'd0;
           weight_n6 = 16'd0;
           weight_n7 = 16'd0;
           weight_n8 = 16'd0;
           weight_n9 = 16'd0;
        end

    endcase
end

// MULTIPLIERS

wire signed [(1+WHOLE+FRACT)-1:0] wi_n0;
wire signed [(1+WHOLE+FRACT)-1:0] wi_n1;
wire signed [(1+WHOLE+FRACT)-1:0] wi_n2;
wire signed [(1+WHOLE+FRACT)-1:0] wi_n3;
wire signed [(1+WHOLE+FRACT)-1:0] wi_n4;
wire signed [(1+WHOLE+FRACT)-1:0] wi_n5;
wire signed [(1+WHOLE+FRACT)-1:0] wi_n6;
wire signed [(1+WHOLE+FRACT)-1:0] wi_n7;
wire signed [(1+WHOLE+FRACT)-1:0] wi_n8;
wire signed [(1+WHOLE+FRACT)-1:0] wi_n9;

wire overflow0;
wire overflow1;
wire overflow2;
wire overflow3;
wire overflow4;
wire overflow5;
wire overflow6;
wire overflow7;
wire overflow8;
wire overflow9;

signed_fxp_mult
#(
    .FRACT(FRACT),
    .WHOLE(WHOLE)
)
mult_n0
(
    .in1(layer_in_ext),
    .in2(weight_n0),
    .out(wi_n0),
    .overflow(overflow0)
);

signed_fxp_mult
#(
    .FRACT(FRACT),
    .WHOLE(WHOLE)
)
mult_n1
(
    .in1(layer_in_ext),
    .in2(weight_n1),
    .out(wi_n1),
    .overflow(overflow1)
);

signed_fxp_mult
#(
    .FRACT(FRACT),
    .WHOLE(WHOLE)
)
mult_n2
(
    .in1(layer_in_ext),
    .in2(weight_n2),
    .out(wi_n2),
    .overflow(overflow2)
);

signed_fxp_mult
#(
    .FRACT(FRACT),
    .WHOLE(WHOLE)
)
mult_n3
(
    .in1(layer_in_ext),
    .in2(weight_n3),
    .out(wi_n3),
    .overflow(overflow3)
);

signed_fxp_mult
#(
    .FRACT(FRACT),
    .WHOLE(WHOLE)
)
mult_n4
(
    .in1(layer_in_ext),
    .in2(weight_n4),
    .out(wi_n4),
    .overflow(overflow4)
);

signed_fxp_mult
#(
    .FRACT(FRACT),
    .WHOLE(WHOLE)
)
mult_n5
(
    .in1(layer_in_ext),
    .in2(weight_n5),
    .out(wi_n5),
    .overflow(overflow5)
);

signed_fxp_mult
#(
    .FRACT(FRACT),
    .WHOLE(WHOLE)
)
mult_n6
(
    .in1(layer_in_ext),
    .in2(weight_n6),
    .out(wi_n6),
    .overflow(overflow6)
);

signed_fxp_mult
#(
    .FRACT(FRACT),
    .WHOLE(WHOLE)
)
mult_n7
(
    .in1(layer_in_ext),
    .in2(weight_n7),
    .out(wi_n7),
    .overflow(overflow7)
);

signed_fxp_mult
#(
    .FRACT(FRACT),
    .WHOLE(WHOLE)
)
mult_n8
(
    .in1(layer_in_ext),
    .in2(weight_n8),
    .out(wi_n8),
    .overflow(overflow8)
);

signed_fxp_mult
#(
    .FRACT(FRACT),
    .WHOLE(WHOLE)
)
mult_n9
(
    .in1(layer_in_ext),
    .in2(weight_n9),
    .out(wi_n9),
    .overflow(overflow9)
);

// ACCUMULATORS

reg signed [(4)+(1+WHOLE+FRACT)-1:0] acc_n0;
reg signed [(4)+(1+WHOLE+FRACT)-1:0] acc_n1;
reg signed [(4)+(1+WHOLE+FRACT)-1:0] acc_n2;
reg signed [(4)+(1+WHOLE+FRACT)-1:0] acc_n3;
reg signed [(4)+(1+WHOLE+FRACT)-1:0] acc_n4;
reg signed [(4)+(1+WHOLE+FRACT)-1:0] acc_n5;
reg signed [(4)+(1+WHOLE+FRACT)-1:0] acc_n6;
reg signed [(4)+(1+WHOLE+FRACT)-1:0] acc_n7;
reg signed [(4)+(1+WHOLE+FRACT)-1:0] acc_n8;
reg signed [(4)+(1+WHOLE+FRACT)-1:0] acc_n9;

wire msb_acc_n0 = acc_n0[(4)+(1+WHOLE+FRACT)-1];
wire msb_acc_n1 = acc_n1[(4)+(1+WHOLE+FRACT)-1];
wire msb_acc_n2 = acc_n2[(4)+(1+WHOLE+FRACT)-1];
wire msb_acc_n3 = acc_n3[(4)+(1+WHOLE+FRACT)-1];
wire msb_acc_n4 = acc_n4[(4)+(1+WHOLE+FRACT)-1];
wire msb_acc_n5 = acc_n5[(4)+(1+WHOLE+FRACT)-1];
wire msb_acc_n6 = acc_n6[(4)+(1+WHOLE+FRACT)-1];
wire msb_acc_n7 = acc_n7[(4)+(1+WHOLE+FRACT)-1];
wire msb_acc_n8 = acc_n8[(4)+(1+WHOLE+FRACT)-1];
wire msb_acc_n9 = acc_n9[(4)+(1+WHOLE+FRACT)-1];

wire [(4)+(WHOLE-4+1)-1:0] trunc_acc_n0 = acc_n0[(4)+(1+WHOLE+FRACT)-1-(1):FRACT+3];
wire [(4)+(WHOLE-4+1)-1:0] trunc_acc_n1 = acc_n1[(4)+(1+WHOLE+FRACT)-1-(1):FRACT+3];
wire [(4)+(WHOLE-4+1)-1:0] trunc_acc_n2 = acc_n2[(4)+(1+WHOLE+FRACT)-1-(1):FRACT+3];
wire [(4)+(WHOLE-4+1)-1:0] trunc_acc_n3 = acc_n3[(4)+(1+WHOLE+FRACT)-1-(1):FRACT+3];
wire [(4)+(WHOLE-4+1)-1:0] trunc_acc_n4 = acc_n4[(4)+(1+WHOLE+FRACT)-1-(1):FRACT+3];
wire [(4)+(WHOLE-4+1)-1:0] trunc_acc_n5 = acc_n5[(4)+(1+WHOLE+FRACT)-1-(1):FRACT+3];
wire [(4)+(WHOLE-4+1)-1:0] trunc_acc_n6 = acc_n6[(4)+(1+WHOLE+FRACT)-1-(1):FRACT+3];
wire [(4)+(WHOLE-4+1)-1:0] trunc_acc_n7 = acc_n7[(4)+(1+WHOLE+FRACT)-1-(1):FRACT+3];
wire [(4)+(WHOLE-4+1)-1:0] trunc_acc_n8 = acc_n8[(4)+(1+WHOLE+FRACT)-1-(1):FRACT+3];
wire [(4)+(WHOLE-4+1)-1:0] trunc_acc_n9 = acc_n9[(4)+(1+WHOLE+FRACT)-1-(1):FRACT+3];


always @(posedge clk or negedge rst_n) begin
    if (!rst_n) begin
        acc_n0 <= bias[0];
        acc_n1 <= bias[1];
        acc_n2 <= bias[2];
        acc_n3 <= bias[3];
        acc_n4 <= bias[4];
        acc_n5 <= bias[5];
        acc_n6 <= bias[6];
        acc_n7 <= bias[7];
        acc_n8 <= bias[8];
        acc_n9 <= bias[9];
    end
    else if (valid_in) begin
        acc_n0 <= bias[0];
        acc_n1 <= bias[1];
        acc_n2 <= bias[2];
        acc_n3 <= bias[3];
        acc_n4 <= bias[4];
        acc_n5 <= bias[5];
        acc_n6 <= bias[6];
        acc_n7 <= bias[7];
        acc_n8 <= bias[8];
        acc_n9 <= bias[9];
    end
    else if (acc_en) begin
        acc_n0 <= acc_n0 + wi_n0;
        acc_n1 <= acc_n1 + wi_n1;
        acc_n2 <= acc_n2 + wi_n2;
        acc_n3 <= acc_n3 + wi_n3;
        acc_n4 <= acc_n4 + wi_n4;
        acc_n5 <= acc_n5 + wi_n5;
        acc_n6 <= acc_n6 + wi_n6;
        acc_n7 <= acc_n7 + wi_n7;
        acc_n8 <= acc_n8 + wi_n8;
        acc_n9 <= acc_n9 + wi_n9;
    end
    else begin
        acc_n0 <= acc_n0;
        acc_n1 <= acc_n1;
        acc_n2 <= acc_n2;
        acc_n3 <= acc_n3;
        acc_n4 <= acc_n4;
        acc_n5 <= acc_n5;
        acc_n6 <= acc_n6;
        acc_n7 <= acc_n7;
        acc_n8 <= acc_n8;
        acc_n9 <= acc_n9;
    end
end
wire [10:0] index_n0 = {msb_acc_n0, acc_n0[(FRACT-1)+3:FRACT-7]};
wire [10:0] index_n1 = {msb_acc_n1, acc_n1[(FRACT-1)+3:FRACT-7]};
wire [10:0] index_n2 = {msb_acc_n2, acc_n2[(FRACT-1)+3:FRACT-7]};
wire [10:0] index_n3 = {msb_acc_n3, acc_n3[(FRACT-1)+3:FRACT-7]};
wire [10:0] index_n4 = {msb_acc_n4, acc_n4[(FRACT-1)+3:FRACT-7]};
wire [10:0] index_n5 = {msb_acc_n5, acc_n5[(FRACT-1)+3:FRACT-7]};
wire [10:0] index_n6 = {msb_acc_n6, acc_n6[(FRACT-1)+3:FRACT-7]};
wire [10:0] index_n7 = {msb_acc_n7, acc_n7[(FRACT-1)+3:FRACT-7]};
wire [10:0] index_n8 = {msb_acc_n8, acc_n8[(FRACT-1)+3:FRACT-7]};
wire [10:0] index_n9 = {msb_acc_n9, acc_n9[(FRACT-1)+3:FRACT-7]};

// if msb=0, only one 1 is enough to point out that a positive overflow occured.
wire ovfpos_trunc_acc_n0 = !msb_acc_n0 && (|trunc_acc_n0);wire ovfpos_trunc_acc_n1 = !msb_acc_n1 && (|trunc_acc_n1);wire ovfpos_trunc_acc_n2 = !msb_acc_n2 && (|trunc_acc_n2);wire ovfpos_trunc_acc_n3 = !msb_acc_n3 && (|trunc_acc_n3);wire ovfpos_trunc_acc_n4 = !msb_acc_n4 && (|trunc_acc_n4);wire ovfpos_trunc_acc_n5 = !msb_acc_n5 && (|trunc_acc_n5);wire ovfpos_trunc_acc_n6 = !msb_acc_n6 && (|trunc_acc_n6);wire ovfpos_trunc_acc_n7 = !msb_acc_n7 && (|trunc_acc_n7);wire ovfpos_trunc_acc_n8 = !msb_acc_n8 && (|trunc_acc_n8);wire ovfpos_trunc_acc_n9 = !msb_acc_n9 && (|trunc_acc_n9);

// if msb=1, only one 0 is enough to point out that a negative overflow occured.
wire ovfneg_trunc_acc_n0 =  msb_acc_n0 && !(&trunc_acc_n0);wire ovfneg_trunc_acc_n1 =  msb_acc_n1 && !(&trunc_acc_n1);wire ovfneg_trunc_acc_n2 =  msb_acc_n2 && !(&trunc_acc_n2);wire ovfneg_trunc_acc_n3 =  msb_acc_n3 && !(&trunc_acc_n3);wire ovfneg_trunc_acc_n4 =  msb_acc_n4 && !(&trunc_acc_n4);wire ovfneg_trunc_acc_n5 =  msb_acc_n5 && !(&trunc_acc_n5);wire ovfneg_trunc_acc_n6 =  msb_acc_n6 && !(&trunc_acc_n6);wire ovfneg_trunc_acc_n7 =  msb_acc_n7 && !(&trunc_acc_n7);wire ovfneg_trunc_acc_n8 =  msb_acc_n8 && !(&trunc_acc_n8);wire ovfneg_trunc_acc_n9 =  msb_acc_n9 && !(&trunc_acc_n9);


// FSM FOR ACTIVATION FUNCTION

reg [4:0] mem_last_state, mem_state, mem_next_state;
always @(posedge clk or negedge rst_n) begin
	if (!rst_n) begin
		mem_state <= IDLE;
		mem_last_state <= IDLE;
	end
	else begin
		mem_state <= mem_next_state;
		mem_last_state <= mem_state;
	end
end

always @(*) begin
	case(mem_state)
		IDLE: if (state == DONE) mem_next_state = ST_1;
			else               mem_next_state = IDLE;
		ST_1: mem_next_state = ST_2;
		ST_2: mem_next_state = ST_3;
		ST_3: mem_next_state = ST_4;
		ST_4: mem_next_state = ST_5;
		ST_5: mem_next_state = ST_6;
		ST_6: mem_next_state = ST_7;
		ST_7: mem_next_state = ST_8;
		ST_8: mem_next_state = ST_9;
		ST_9: mem_next_state = ST_10;
		ST_10: mem_next_state = DONE;
		DONE:       mem_next_state = IDLE;
		default:    mem_next_state = IDLE;
	endcase
end

// ROM INPUT SELECTION (FROM ACCUMULATORS)

// reg [10:0] mem_index;
// reg mem_rd_en;
always @(*) begin
	case(mem_next_state)
		IDLE:   begin
	mem_index = 11'd0;
		end
		ST_1: begin
			mem_index = index_n0;
		end
		ST_2: begin
			mem_index = index_n1;
		end
		ST_3: begin
			mem_index = index_n2;
		end
		ST_4: begin
			mem_index = index_n3;
		end
		ST_5: begin
			mem_index = index_n4;
		end
		ST_6: begin
			mem_index = index_n5;
		end
		ST_7: begin
			mem_index = index_n6;
		end
		ST_8: begin
			mem_index = index_n7;
		end
		ST_9: begin
			mem_index = index_n8;
		end
		ST_10: begin
			mem_index = index_n9;
		end
		DONE:   begin
	mem_index = 11'd0;
		end
		default:begin
	mem_index = 11'd0;
		end
	endcase
end

reg [8:0] layer_out;
always @(*) begin
	case(mem_last_state)
		IDLE: layer_out = 9'd0;
		ST_1: layer_out = ovfpos_trunc_acc_n0 ? {9{1'b1}} : (ovfneg_trunc_acc_n0 ? {9{1'b0}} : mem_out);
		ST_2: layer_out = ovfpos_trunc_acc_n1 ? {9{1'b1}} : (ovfneg_trunc_acc_n1 ? {9{1'b0}} : mem_out);
		ST_3: layer_out = ovfpos_trunc_acc_n2 ? {9{1'b1}} : (ovfneg_trunc_acc_n2 ? {9{1'b0}} : mem_out);
		ST_4: layer_out = ovfpos_trunc_acc_n3 ? {9{1'b1}} : (ovfneg_trunc_acc_n3 ? {9{1'b0}} : mem_out);
		ST_5: layer_out = ovfpos_trunc_acc_n4 ? {9{1'b1}} : (ovfneg_trunc_acc_n4 ? {9{1'b0}} : mem_out);
		ST_6: layer_out = ovfpos_trunc_acc_n5 ? {9{1'b1}} : (ovfneg_trunc_acc_n5 ? {9{1'b0}} : mem_out);
		ST_7: layer_out = ovfpos_trunc_acc_n6 ? {9{1'b1}} : (ovfneg_trunc_acc_n6 ? {9{1'b0}} : mem_out);
		ST_8: layer_out = ovfpos_trunc_acc_n7 ? {9{1'b1}} : (ovfneg_trunc_acc_n7 ? {9{1'b0}} : mem_out);
		ST_9: layer_out = ovfpos_trunc_acc_n8 ? {9{1'b1}} : (ovfneg_trunc_acc_n8 ? {9{1'b0}} : mem_out);
		ST_10: layer_out = ovfpos_trunc_acc_n9 ? {9{1'b1}} : (ovfneg_trunc_acc_n9 ? {9{1'b0}} : mem_out);
		DONE: layer_out = 9'd0;
		default: layer_out = 9'd0;
	endcase
end

wire valid_out = (mem_state == ST_1);

always @(posedge clk or negedge rst_n) begin
	if (!rst_n) begin
		layer_out_delayed <= 8'd0;
		delayed_valid_out <= 1'd0;
	end
	else begin
		layer_out_delayed <= layer_out;
		delayed_valid_out <= valid_out;
	end
end

endmodule
