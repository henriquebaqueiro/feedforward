# Constrain clock port clk with a 10-ns requirement

create_clock -period 20 [get_ports clk]

create_generated_clock -name clk_25 -source [get_ports {clk}] -divide_by 2

set_clock_uncertainty -from { clk } -to { clk } -setup 1
set_clock_uncertainty -from { clk } -to { clk } -hold 1

create_generated_clock -name vga_clk -source [get_ports {clk}] -divide_by 2
create_generated_clock -name cam_xclk -source [get_ports {clk}] -divide_by 2
create_generated_clock -name cam_pclk -source [get_ports {clk}] -divide_by 2

# Automatically apply a generate clock on the output of phase-locked loops (PLLs) 
# This command can be safely left in the SDC even if no PLLs exist in the design

derive_pll_clocks

# Constrain the input I/O path

set_input_delay -clock clk -max 3 [all_inputs]

set_input_delay -clock clk -min 2 [all_inputs]

# Constrain the output I/O path

set_output_delay -clock clk -max 3 [all_outputs]

set_output_delay -clock clk -min 2 [all_outputs]
