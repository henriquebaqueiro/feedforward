//**********************************************************************//
//------------------- UNIVERSIDADE FEDERAL DA BAHIA --------------------//
//--------------------- Escola Politécnica da UFBa ---------------------//
//---------- Programa de Pós-Graduação em Engenharia Elétrica ----------//
//                                                                      //
// File name            : ov7670_vga_capture.v                          //
// File contents        : OV7670_VGA_Capture captures the luminance Y   //
//                        components of the full screen (640x480) from  //
//                        the camera and selects the region of interest //
//                        (28x28) for further processing. Both are      //
//                        displayed.                                    //
//                                                                      //
// Design Engineer      : Henrique Baqueiro                             //
// Last Changed         : 14 July 2018 (version 0.7)                    //
//                                                                      //
//----------------------------------------------------------------------//
//**********************************************************************//

`timescale 1 ps / 1 ps

module ov7670_vga_capture
(
  input clk,            // 50MHz
  input rst_n,

  // VGA Interface
  output vga_clk,
  output vga_sync,      // always high
  output vga_blank,     // always high

  output vga_hsync,
  output vga_vsync,

  output [7:0] vga_R,
  output [7:0] vga_G,
  output [7:0] vga_B,

  // Camera Interface
  output cam_xclk,      // Cam System Clock (25MHz)
  output cam_rst_n,     // always high (active low)
  output cam_pwdn_p,    // always low (active high)

  input cam_pclk,       // Cam Pixel Clock (same frequency as cam_xclk)
  input cam_vsync,
  input cam_href,

  input cam_d0,
  input cam_d1,
  input cam_d2,
  input cam_d3,
  input cam_d4,
  input cam_d5,
  input cam_d6,
  input cam_d7,

  // Test pins
  input switch
);

reg clk_25;         // 25MHz
always @(posedge clk or negedge rst_n) begin
  if (!rst_n) clk_25 <= 1'b0;
  else        clk_25 <= !clk_25;
end

assign vga_clk   = clk_25;
assign vga_sync  = 1'b1;
assign vga_blank = 1'b1;


localparam POS_LENGTH = 4'd10;
localparam [POS_LENGTH-1:0] H_VISIBLE  = 10'd640, V_VISIBLE = 10'd480,
                            H_FPORCH   = 10'd16,  V_FPORCH  = 10'd10,
                            H_SYNC     = 10'd96,  V_SYNC    = 10'd2,
                            H_BPORCH   = 10'd48,  V_BPORCH  = 10'd33;

localparam [POS_LENGTH-1:0] H_OFFSET   = H_FPORCH + H_BPORCH + H_SYNC;
localparam [POS_LENGTH-1:0] V_OFFSET   = V_FPORCH + V_BPORCH + V_SYNC;
localparam [POS_LENGTH-1:0] H_MAX      = H_OFFSET + H_VISIBLE;
localparam [POS_LENGTH-1:0] V_MAX      = V_OFFSET + V_VISIBLE;


wire [POS_LENGTH-1:0] hpos, vpos;
vga_sync vga_sync_u0
(
  .clk(clk_25),
  .rst_n(rst_n),
  .hsync(vga_hsync),
  .vsync(vga_vsync),
  .hpos(hpos),
  .vpos(vpos)
);

wire [7:0] data_to_vga;
wire [7:0] cam_data = {cam_d7, cam_d6, cam_d5, cam_d4, cam_d3, cam_d2, cam_d1, cam_d0};

assign cam_xclk   = clk_25;
assign cam_rst_n  = 1'b1; // active low
assign cam_pwdn_p = 1'b0; // active high

// Limits the full display borders
/*wire border_mark = (hpos == H_OFFSET) ||
                   (hpos == H_MAX-1'b1) ||
                   (vpos == V_OFFSET)   ||
                   (vpos == V_MAX-1'b1);*/

// Limits 28x28 pixels in the center of the image
wire center_mark = (hpos == H_OFFSET + 10'd306) ||
                   (hpos == H_OFFSET + 10'd335) ||
                   (vpos == V_OFFSET + 10'd226) ||
                   (vpos == V_OFFSET + 10'd255);

wire red_marks = center_mark; // border_mark || center_mark;

// Camera Region of Interest (28x28)
wire cam_roi =  (col_counter > 10'd306) &&
                (col_counter < 10'd335) &&
                (row_counter > 10'd226) &&
                (row_counter < 10'd255);

// Screen Valid Area (visible pixels)
wire scr_vld_area = (((hpos >= H_OFFSET) && (hpos < H_MAX)) &&
                     ((vpos >= V_OFFSET) && (vpos < V_MAX)));

// Data to VGA are either the red_marks, output of the small memory or output of the big memory
assign vga_R = scr_vld_area ? (red_marks ? 8'd255 : (roi_area ? out_roi : data_to_vga)) : 8'd0;
assign vga_G = scr_vld_area ? (red_marks ? 8'd0   : (roi_area ? out_roi : data_to_vga)) : 8'd0;
assign vga_B = scr_vld_area ? (red_marks ? 8'd0   : (roi_area ? out_roi : data_to_vga)) : 8'd0;

// VGA area where camera ROI will be displayed
wire roi_area = (hpos > H_MAX - 10'd39) &&
                (hpos < H_MAX - 10'd10) &&
                (vpos > V_MAX - 10'd39) &&
                (vpos < V_MAX - 10'd10);

// Needed for timing purposes
wire pre_roi_area = (hpos > H_MAX - 10'd40) && // |
                    (hpos < H_MAX - 10'd11) && //    |
                    (vpos > V_MAX - 10'd39) && //  ^
                    (vpos < V_MAX - 10'd10);   //  _

// Just a counter for reading contents from the big memory (full image from scr_vld_area)
reg [18:0] rd_addr_full;    // 19bits for 640x480 addresses

always @(posedge clk_25 or negedge rst_n) begin
  if (!rst_n) begin
    rd_addr_full <= 19'd1;  // avoids wrap-around
  end
  else if (scr_vld_area) begin
    if (rd_addr_full == 19'd307199) rd_addr_full <= 19'b0;
    else                            rd_addr_full <= rd_addr_full + 1'b1;
  end
end

// Just a counter for reading contents from the small memory (Region Of Interest or roi_area)
reg  [9:0] rd_addr_roi; // 10bits for 28x28 addresses

always @(posedge clk_25 or negedge rst_n) begin
  if (!rst_n) begin
    rd_addr_roi <= 10'd0;
  end
  else if (pre_roi_area) begin
    if (rd_addr_roi == 10'd783) rd_addr_roi <= 10'b0;
    else                        rd_addr_roi <= rd_addr_roi + 1'b1;
  end
end


// First thing to notice, the D0-D7 must be sampled at the rising edge of the
// PCLK signal. Number two, D0-D7 must be sampled only when HREF is high. Also,
// the rising edge of HREF signals the start of a line, and the falling edge of
// HREF signals the end of the line.
// All these bytes sampled when HREF was high, correspond to the pixels in one
// line. Note that one byte is not a pixel, it depends on the format chosen. By
// default, the format is YCbCr422, this means that in average two bytes
// correspond to a pixel.

reg x;                    // only luminance bytes
reg wr_en_full;
reg [7:0]  wr_data_full;
reg [18:0] wr_addr_full;  // 19bits for 640x480 addresses

always @(posedge cam_pclk or negedge rst_n) begin
  if (!rst_n) begin
    x <= 1'b0;
    wr_en_full <= 1'b0;
    wr_data_full <= 8'b0;
    wr_addr_full <= 19'd307199;
  end
  else begin
    if (cam_vsync) begin
      x <= 1'b0;
      wr_en_full <= 1'b0;
      wr_addr_full <= 19'd0;
    end
    else if (cam_href) begin
      x <= !x;
      if (x && !switch) begin
        wr_en_full <= 1'b1;
        wr_data_full <= cam_data;
        if (wr_addr_full == 19'd307199) wr_addr_full <= 19'd0;
        else                            wr_addr_full <= wr_addr_full + 1'b1;
      end
      else wr_en_full <= 1'b0;
    end
    else wr_en_full <= 1'b0;
  end
end


// Register current VSYNC and HREF to indentify its borders.
reg last_cam_href;
reg last_cam_vsync;

always @(posedge cam_pclk or negedge rst_n) begin
  if (!rst_n) begin
    last_cam_href  <= 1'b0;
    last_cam_vsync <= 1'b0;
  end
  else begin
    last_cam_href  <= cam_href;
    last_cam_vsync <= cam_vsync;
  end
end

// The falling edge of VSYNC signals the start of a frame
// (and its rising edge signals the end of a frame)
wire cam_vsync_fall = !cam_vsync &&  last_cam_vsync;
// The rising edge of HREF signals the start of a line.
wire cam_href_rise  =  cam_href  && !last_cam_href;
// The falling edge of HREF signals the end of the line.
wire cam_href_fall  = !cam_href  &&  last_cam_href;


// Count rows and columns of visible pixels to identify the Region of Interest
reg y;                    // only luminance bytes
reg [9:0] row_counter;
reg [9:0] col_counter;

always @(posedge cam_pclk or negedge rst_n) begin
  if (!rst_n) begin
    y <= 1'b0;
    row_counter <= 10'b0;
    col_counter <= 10'b0;
  end
  else begin
    if (cam_vsync_fall) begin
      y <= 1'b0;
      row_counter <= 10'b0;
      col_counter <= 10'b0;
    end
    else if (cam_href_fall) begin
      col_counter <= 10'b0;
    end
    else if (cam_href) begin
      y <= !y;
      if (cam_href_rise) row_counter <= row_counter + 1'b1; // 480 rows in a frame
      if (!y)            col_counter <= col_counter + 1'b1; // 640 columns in a row
    end
  end
end


// Big memory (600x480 bytes) for full image (visible pixels)
imgRAM_307200x8bits_2port	imgRAM_307200x8bits_2port_inst
(
  // read port
	.rdclock (clk_25),
	.rdaddress (rd_addr_full),
	.q (data_to_vga),

  // write port
	.wrclock (cam_pclk),
	.wren (wr_en_full),
	.data (wr_data_full),
	.wraddress (wr_addr_full)
);

reg z;                    // only luminance bytes
reg wr_en_roi;
reg [9:0] wr_addr_roi;    // 10bits for 28x28 addresses
reg [7:0] wr_data_roi;
wire [7:0] out_roi;

always @(posedge cam_pclk or negedge rst_n) begin
  if (!rst_n) begin
    z <= 1'b0;
    wr_en_roi <= 1'b0;
    wr_addr_roi <= 10'd783;
    wr_data_roi <= 8'b0;
  end
  else begin
    if (cam_roi) begin
      z <= !z;
      if (!z && !switch) begin
        wr_en_roi <= 1'b1;
        wr_data_roi <= cam_data;
        if (wr_addr_roi == 10'd783) wr_addr_roi <= 10'd0;
        else                        wr_addr_roi <= wr_addr_roi + 1'b1;
      end
    end
    else wr_en_roi <= 1'b0;
  end
end

// Small memory (28x28 bytes) for Region of Interest
imgRAM_784x8bits_2port	imgRAM_784x8bits_2port_inst
(
  // read port
  .rdclock (clk_25),
  .rdaddress (rd_addr_roi),
  .q (out_roi),

  // write port
  .wrclock (cam_pclk),	
  .wren (wr_en_roi),
  .data (wr_data_roi),   
  .wraddress (wr_addr_roi)
);

endmodule
