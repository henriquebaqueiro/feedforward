// File generated at 2019-11-22 20:59:13.

module hardmax
#(
	parameter integer DATA_LEN = 8
)
(
	input clk,
	input rst_n,
	input start,
	input [DATA_LEN-1:0] data_in,
	output reg [3:0] index,
	output ready
);

reg [DATA_LEN-1:0] reg_0;
reg [DATA_LEN-1:0] reg_1;
reg [DATA_LEN-1:0] reg_2;
reg [DATA_LEN-1:0] reg_3;
reg [DATA_LEN-1:0] reg_4;
reg [DATA_LEN-1:0] reg_5;
reg [DATA_LEN-1:0] reg_6;
reg [DATA_LEN-1:0] reg_7;
reg [DATA_LEN-1:0] reg_8;
reg [DATA_LEN-1:0] reg_9;

// FSM Parameters
localparam SIZE = 4;
localparam IDLE = 4'd0,
	ST_1 = 4'd1,
	ST_2 = 4'd2,
	ST_3 = 4'd3,
	ST_4 = 4'd4,
	ST_5 = 4'd5,
	ST_6 = 4'd6,
	ST_7 = 4'd7,
	ST_8 = 4'd8,
	ST_9 = 4'd9,
	ST_10 = 4'd10,
	ST_11 = 4'd11,
	DONE = 4'd12;

// FSM Sequential Logic
reg [SIZE-1:0] state, next_state;
always@(posedge clk or negedge rst_n) begin
	if (!rst_n) state <= IDLE;
	else state <= next_state;
end

// FSM Next State Logic
always@(*) begin
	case(state)
		IDLE: begin
			if (start) next_state <= ST_1;
			else next_state <= IDLE;
		end
		ST_1: next_state <= ST_2;
		ST_2: next_state <= ST_3;
		ST_3: next_state <= ST_4;
		ST_4: next_state <= ST_5;
		ST_5: next_state <= ST_6;
		ST_6: next_state <= ST_7;
		ST_7: next_state <= ST_8;
		ST_8: next_state <= ST_9;
		ST_9: next_state <= ST_10;
		ST_10: next_state <= ST_11;
		ST_11: next_state <= DONE;
		DONE: next_state <= IDLE;
		default: next_state <= state;
	endcase
end

// FSM Output Logic
always@(posedge clk or negedge rst_n) begin
	if (!rst_n) begin
		reg_0 <= {(DATA_LEN){1'b0}};
		reg_1 <= {(DATA_LEN){1'b0}};
		reg_2 <= {(DATA_LEN){1'b0}};
		reg_3 <= {(DATA_LEN){1'b0}};
		reg_4 <= {(DATA_LEN){1'b0}};
		reg_5 <= {(DATA_LEN){1'b0}};
		reg_6 <= {(DATA_LEN){1'b0}};
		reg_7 <= {(DATA_LEN){1'b0}};
		reg_8 <= {(DATA_LEN){1'b0}};
		reg_9 <= {(DATA_LEN){1'b0}};
	end
	else if (state == DONE) begin
		reg_0 <= {(DATA_LEN){1'b0}};
		reg_1 <= {(DATA_LEN){1'b0}};
		reg_2 <= {(DATA_LEN){1'b0}};
		reg_3 <= {(DATA_LEN){1'b0}};
		reg_4 <= {(DATA_LEN){1'b0}};
		reg_5 <= {(DATA_LEN){1'b0}};
		reg_6 <= {(DATA_LEN){1'b0}};
		reg_7 <= {(DATA_LEN){1'b0}};
		reg_8 <= {(DATA_LEN){1'b0}};
		reg_9 <= {(DATA_LEN){1'b0}};
	end
	else begin
		case(state)
			ST_1:  reg_0 <= data_in;
			ST_2:  reg_1 <= data_in;
			ST_3:  reg_2 <= data_in;
			ST_4:  reg_3 <= data_in;
			ST_5:  reg_4 <= data_in;
			ST_6:  reg_5 <= data_in;
			ST_7:  reg_6 <= data_in;
			ST_8:  reg_7 <= data_in;
			ST_9:  reg_8 <= data_in;
			ST_10:  reg_9 <= data_in;
		endcase
	end
end

reg [DATA_LEN-1:0] max;
always@(posedge clk or negedge rst_n) begin
	if (!rst_n) begin
		index <= {(4){1'b0}};
		max <= {(DATA_LEN){1'b0}};
	end
	else if (state == DONE) begin
		index <= {(4){1'b0}};
		max <= {(DATA_LEN){1'b0}};
	end
	else begin
		case(state)
			ST_3:  begin
				if (reg_1 > reg_0) begin
					index <= 4'd1;
					max <= reg_1;
				end
				else begin
					index <= 4'd0;
					max <= reg_0;
				end
			end
			ST_4:  begin
				if (reg_2 > max) begin
					max <= reg_2;
					index <= 4'd2;
				end
			end
			ST_5:  begin
				if (reg_3 > max) begin
					max <= reg_3;
					index <= 4'd3;
				end
			end
			ST_6:  begin
				if (reg_4 > max) begin
					max <= reg_4;
					index <= 4'd4;
				end
			end
			ST_7:  begin
				if (reg_5 > max) begin
					max <= reg_5;
					index <= 4'd5;
				end
			end
			ST_8:  begin
				if (reg_6 > max) begin
					max <= reg_6;
					index <= 4'd6;
				end
			end
			ST_9:  begin
				if (reg_7 > max) begin
					max <= reg_7;
					index <= 4'd7;
				end
			end
			ST_10:  begin
				if (reg_8 > max) begin
					max <= reg_8;
					index <= 4'd8;
				end
			end
			ST_11:  begin
				if (reg_9 > max) begin
					max <= reg_9;
					index <= 4'd9;
				end
			end
		endcase
	end
end

assign ready = (state == DONE);

endmodule
