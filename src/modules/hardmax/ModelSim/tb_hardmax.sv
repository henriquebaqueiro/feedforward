`timescale 1ns/100ps
module tb_hardmax;

parameter INPUT_LEN = 8;

reg  clk;
reg  rst_n;

reg [INPUT_LEN-1:0] in0;
reg [INPUT_LEN-1:0] in1;
reg [INPUT_LEN-1:0] in2;
reg [INPUT_LEN-1:0] in3;
reg [INPUT_LEN-1:0] in4;
reg [INPUT_LEN-1:0] in5;
reg [INPUT_LEN-1:0] in6;
reg [INPUT_LEN-1:0] in7;
reg [INPUT_LEN-1:0] in8;
reg [INPUT_LEN-1:0] in9;

wire [INPUT_LEN-1:0] max;
wire [3:0] index;

hardmax
#(
  .INPUT_LEN(INPUT_LEN)
)
hardmax_u0
(
  .in0(in0),
  .in1(in1),
  .in2(in2),
  .in3(in3),
  .in4(in4),
  .in5(in5),
  .in6(in6),
  .in7(in7),
  .in8(in8),
  .in9(in9),
  .max(max),
  .index(index)
);

initial clk = 1'b0;
always #(1) clk = ~clk;

integer i;

initial begin
  // 1
  feed_inputs;
  rst_n = 1;
  @(negedge clk);
  rst_n = 0;
  @(negedge clk);
  rst_n = 1;
  @(negedge clk);

  for (i=0; i<20; i=i+1) begin
    feed_inputs;
    @(negedge clk);
  end

  repeat (5) @(negedge clk);
  $finish;
end

task feed_inputs;
  begin
    in0 = $urandom & 15; //4 bit random number
    in1 = $urandom & 15;
    in2 = $urandom & 15;
    in3 = $urandom & 15;
    in4 = $urandom & 15;
    in5 = $urandom & 15;
    in6 = $urandom & 15;
    in7 = $urandom & 15;
    in8 = $urandom & 15;
    in9 = $urandom & 15;
  end
endtask

endmodule
