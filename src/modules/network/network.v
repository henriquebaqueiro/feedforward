// File generated at 2019-11-22 20:59:13.

module network
#(
	parameter FRACT = 13,
	parameter WHOLE = 2,
	parameter N = 9
)
(
	input clk,
	input rst_n,
	input [N-1:0] net_in,
	input valid_in,
	output [3:0] net_classification,
	output net_valid_out
);

wire [N-1:0] out_l1;
wire [N-1:0] out_l2;
wire [N-1:0] out_l3;

wire valid_out_l1;
wire valid_out_l2;
wire valid_out_l3;

wire [10:0] mem_index_l1;
wire [10:0] mem_index_l2;
wire [10:0] mem_index_l3;

wire [10:0] mem_index = mem_index_l1 | mem_index_l2 | mem_index_l3;

wire [8:0] mem_out;

activationROM_2048x9bits activationROM_2048x9bits_inst
(	.clock (clk),
	.address (mem_index),
	.q (mem_out)
);

layer1 #(
	.FRACT(FRACT),
	.WHOLE(WHOLE)
)
layer1_inst
(
	.clk(clk),
	.rst_n(rst_n),
	.layer_in(net_in),
	.valid_in(valid_in),
	.mem_index(mem_index_l1),
	.mem_out(mem_out),
	.layer_out_delayed(out_l1),
	.delayed_valid_out(valid_out_l1)
);

layer2 #(
	.FRACT(FRACT),
	.WHOLE(WHOLE)
)
layer2_inst
(
	.clk(clk),
	.rst_n(rst_n),
	.layer_in(out_l1),
	.valid_in(valid_out_l1),
	.mem_index(mem_index_l2),
	.mem_out(mem_out),
	.layer_out_delayed(out_l2),
	.delayed_valid_out(valid_out_l2)
);

layer3 #(
	.FRACT(FRACT),
	.WHOLE(WHOLE)
)
layer3_inst
(
	.clk(clk),
	.rst_n(rst_n),
	.layer_in(out_l2),
	.valid_in(valid_out_l2),
	.mem_index(mem_index_l3),
	.mem_out(mem_out),
	.layer_out_delayed(out_l3),
	.delayed_valid_out(valid_out_l3)
);

hardmax
#(
	.DATA_LEN(N)
)
hardmax_inst
(
	.clk(clk),
	.rst_n(rst_n),
	.start(valid_out_l3),
	.data_in(out_l3),
	.index(net_classification),
	.ready(net_valid_out)
);


endmodule
