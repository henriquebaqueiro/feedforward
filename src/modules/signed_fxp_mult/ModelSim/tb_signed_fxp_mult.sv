`timescale 1ns/100ps
module tb_signed_fxp_mult;

parameter FRACT = 8;
parameter WHOLE = 8;

reg  signed [(1+WHOLE+FRACT)-1:0] in1, in2;
wire signed [(1+WHOLE+FRACT)-1:0] out;
wire overflow;


signed_fxp_mult
#(
  .FRACT(FRACT),
  .WHOLE(WHOLE)
)
m0
(
  .in1(in1),
  .in2(in2),
  .out(out),
  .overflow(overflow)
);


initial begin
  in1 = {1'b0, {WHOLE{1'b0}}, 2'b11, {(FRACT-2){1'b0}}};  // 0.75  -->  1.5
  in2 = {1'b0, {(WHOLE-2){1'b0}}, 2'b10, {FRACT{1'b0}}};  // 2
  #100;
  in1 = {1'b0, {WHOLE{1'b0}}, 1'b1, {(FRACT-1){1'b0}}};   // 0.5   --> -0.5
  in2 = {1'b1, {WHOLE{1'b1}}, {FRACT{1'b0}}};             // -1
  #100;
  in1 = {1'b1, {WHOLE{1'b1}}, 2'b01, {(FRACT-2){1'b0}}};  // -0.75 --> -2.25
  in2 = {1'b0, {(WHOLE-2){1'b0}}, 2'b11, {FRACT{1'b0}}};  // 3
  #100;
  in1 = {1'b1, {WHOLE{1'b1}}, 1'b1, {(FRACT-1){1'b0}}};   // -0.5  -->  1.5
  in2 = {1'b1, {{(WHOLE-2){1'b1}}, 2'b01, {FRACT{1'b0}}}};// -3
  #100;
  
  // provoke overflow:
  in1 = 17'b0_01000000_00000000;   // 64
  in2 = 17'b0_00000100_00000000;   //  4
  #100;
  
  in1 = 17'b1_11000000_00000000;   // -64
  in2 = 17'b0_00001000_00000000;   //   8
  #100;

  $stop;
end

localparam F = (2.0**-FRACT);

always @(*) begin
  $display("%f * %f = %f\t\tExpected = %f", in1*F, in2*F, out*F, in1*F*in2*F);
end

real in1_real, in2_real, out_real;
always @(*) begin
  in1_real = in1*F;
  in2_real = in2*F;
  out_real = out*F;
end

endmodule
