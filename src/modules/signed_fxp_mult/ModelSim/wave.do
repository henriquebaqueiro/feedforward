onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_signed_fxp_mult/FRACT
add wave -noupdate /tb_signed_fxp_mult/WHOLE
add wave -noupdate /tb_signed_fxp_mult/F
add wave -noupdate /tb_signed_fxp_mult/in1
add wave -noupdate /tb_signed_fxp_mult/in2
add wave -noupdate /tb_signed_fxp_mult/out
add wave -noupdate /tb_signed_fxp_mult/overflow
add wave -noupdate /tb_signed_fxp_mult/in1_real
add wave -noupdate /tb_signed_fxp_mult/in2_real
add wave -noupdate /tb_signed_fxp_mult/out_real
add wave -noupdate -expand -group inst -radix binary /tb_signed_fxp_mult/m0/out_signal
add wave -noupdate -expand -group inst -radix binary /tb_signed_fxp_mult/m0/out_number
add wave -noupdate -expand -group inst -radix binary /tb_signed_fxp_mult/m0/out_trunc_w
add wave -noupdate -color Magenta -radix binary /tb_signed_fxp_mult/m0/overflow
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 149
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {630 ns}
