//*******************************************************************//
//-------------------------------------------------------------------//
//
// File name            : signed_fxp_mult.v
// File contents        : Signed Fixed-Point Multiplier. Output is 
//                        truncated to the same size of inputs. 
//                        Number of bits of whole and fractional part 
//                        are parametrizable.
// Design Engineer      : Henrique Baqueiro
// Last Changed         : May 30th, 2017
//
//-------------------------------------------------------------------//
//*******************************************************************//

module signed_fxp_mult
#(
  parameter FRACT = 8,
  parameter WHOLE = 8
)
(
  input  signed [(1+WHOLE+FRACT)-1:0] in1,
  input  signed [(1+WHOLE+FRACT)-1:0] in2,
  output signed [(1+WHOLE+FRACT)-1:0] out,
  output overflow // overflow port is important for debug reasons,
                  // in final design it may be suppressed to save hardware.
);

localparam FULL = 1'b1 + WHOLE + FRACT;

wire signed [2*FULL-1:0] result = in1 * in2;

wire out_signal = result[2*FULL-1];
wire [WHOLE+FRACT-1:0] out_number = result[WHOLE+2*FRACT-1:FRACT];

assign out = {out_signal, out_number};

// just for debug
wire [1+WHOLE-1:0] out_trunc_w = result[(2*(1+WHOLE+FRACT)-1)+(-1) : WHOLE+2*FRACT];
// wire [FRACT-1:0] out_trunc_f = result[FRACT-1:0];
// wire [2*FULL-1:0] result_reconstructed = {out_signal, out_trunc_w, out_number, out_trunc_f};
assign overflow = out_signal ? (out_signal)^(&out_trunc_w) : (out_signal)^(|out_trunc_w);

endmodule
