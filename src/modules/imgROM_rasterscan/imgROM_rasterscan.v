// Verified Module

module imgROM_rasterscan
(
    input clk,
    input rst_n,
    input valid_in,
    output [7:0] q,
    output [9:0] q_addr,
    output reg valid_content
);

reg last_valid_in;
wire valid_in_pulse = !last_valid_in && valid_in;

always @(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
        last_valid_in <= 1'b0;
    end
    else begin
        last_valid_in <= valid_in;
    end
end

parameter IDLE  = 3'b001,
          COUNT = 3'b010,
          DONE  = 3'b100;

reg [2:0] state;
reg [2:0] next_state;

reg [9:0] addr;
reg [9:0] next_addr;
wire last_address = (addr == 10'd783);

always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) state <= IDLE;
    else        state <= next_state;
end

always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) addr <= 10'b0;
    else        addr <= next_addr;
end

reg [9:0] addr_d1; // delayed version of addr
reg [9:0] addr_d2; // delayed version of addr_d1
always @(posedge clk or negedge rst_n) begin
    if (!rst_n) begin
        addr_d1 <= 10'd0;
        addr_d2 <= 10'd0;
    end
    else begin
        addr_d1 <= addr;
        addr_d2 <= addr_d1;
    end
end

assign q_addr = addr_d2;

always @(*) begin
    case(state)

        IDLE:   
        begin
            if (valid_in_pulse == 1'b1) begin
                next_state = COUNT;
            end
            else begin
                next_state = IDLE;
            end
            next_addr = 10'b0;
        end

        COUNT:
        begin
            if (last_address) begin
                next_state = DONE;
                next_addr = 10'b0;
            end
            else begin
                next_state = COUNT;
                next_addr = addr + 1'b1;
            end
        end

        DONE:
        begin
            next_state = IDLE;
            next_addr = 10'b0;
        end

        default:
        begin
            next_state = IDLE;
            next_addr = 10'b0;
        end

    endcase
end

wire rden = (state == COUNT);
reg  rden_d1; // delayed version of rden

always @(posedge clk or negedge rst_n) begin
    if (!rst_n) begin
        rden_d1 <= 1'b0;
        valid_content <= 1'b0;
    end
    else begin
        rden_d1 <= rden;
        valid_content <= rden_d1;
    end
end

wire [7:0] q_rom;
imgROM_784x8bits imgROM_784x8bits_inst (
    .clock (clk),
    .rden (rden),
    .address (addr),
    .q (q_rom)
);

assign q = (valid_content) ? q_rom : 8'b0;


endmodule