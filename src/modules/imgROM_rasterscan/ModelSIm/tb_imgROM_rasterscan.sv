`timescale 1ns/100ps
module tb_imgROM_rasterscan;

reg clk;
initial clk = 1'b0;
always #(1) clk = ~clk;

reg rst_n;
initial begin
  rst_n = 1;
  @(negedge clk);
  rst_n = 0;
  @(negedge clk);
  rst_n = 1;
end

reg valid_in;
initial begin
    valid_in = 0;
    repeat (3) @(negedge clk);
    valid_in = 1;
    @(negedge clk);
    valid_in = 0;
end

wire [7:0] q;
wire [9:0] q_addr;
wire valid_content;
imgROM_rasterscan imgROM_rasterscan_inst (
    .clk(clk),
    .rst_n(rst_n),
    .valid_in(valid_in),
    .q(q),
    .q_addr(q_addr),
    .valid_content(valid_content)
);

initial begin
  repeat (2) @(negedge clk);
  @(negedge valid_content);
  repeat (2) @(negedge clk);
  $stop;
end

endmodule