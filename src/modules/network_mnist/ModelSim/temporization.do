onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix binary /tb_network_mnist/network_inst/layer1_inst/clk
add wave -noupdate -radix binary /tb_network_mnist/network_inst/layer1_inst/rst_n
add wave -noupdate -radix binary /tb_network_mnist/valid_out
add wave -noupdate -radix unsigned /tb_network_mnist/clk_counter
add wave -noupdate -color White -radix unsigned /tb_network_mnist/network_inst/layer1_inst/valid_in
add wave -noupdate -color White -radix unsigned /tb_network_mnist/network_inst/layer1_inst/valid_out
add wave -noupdate -color Orange -radix unsigned /tb_network_mnist/network_inst/layer2_inst/valid_in
add wave -noupdate -color Orange -radix unsigned /tb_network_mnist/network_inst/layer2_inst/valid_out
add wave -noupdate -color {Cornflower Blue} -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/start
add wave -noupdate -color {Cornflower Blue} -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/ready
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1734632 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 116
configure wave -valuecolwidth 39
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {1704792 ps} {1736590 ps}
