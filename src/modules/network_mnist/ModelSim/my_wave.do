onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix binary /tb_network_mnist/network_inst/layer1_inst/clk
add wave -noupdate -radix binary /tb_network_mnist/network_inst/layer1_inst/rst_n
add wave -noupdate -radix binary /tb_network_mnist/valid_out
add wave -noupdate -group layer_1 -radix binary /tb_network_mnist/network_inst/layer1_inst/valid_in
add wave -noupdate -group layer_1 -radix unsigned /tb_network_mnist/network_inst/layer1_inst/layer_in
add wave -noupdate -group layer_1 -radix binary /tb_network_mnist/network_inst/layer1_inst/layer_in_ext
add wave -noupdate -group layer_1 -radix unsigned /tb_network_mnist/network_inst/layer1_inst/layer_out
add wave -noupdate -group layer_1 -radix binary /tb_network_mnist/network_inst/layer1_inst/valid_out
add wave -noupdate -group layer_1 -group FSM -radix unsigned /tb_network_mnist/network_inst/layer1_inst/state
add wave -noupdate -group layer_1 -group FSM -radix unsigned /tb_network_mnist/network_inst/layer1_inst/next_state
add wave -noupdate -group layer_1 -group weight /tb_network_mnist/network_inst/layer1_inst/weight_n0
add wave -noupdate -group layer_1 -group weight /tb_network_mnist/network_inst/layer1_inst/weight_n1
add wave -noupdate -group layer_1 -group weight /tb_network_mnist/network_inst/layer1_inst/weight_n2
add wave -noupdate -group layer_1 -group weight /tb_network_mnist/network_inst/layer1_inst/weight_n3
add wave -noupdate -group layer_1 -group weight /tb_network_mnist/network_inst/layer1_inst/weight_n4
add wave -noupdate -group layer_1 -group weight /tb_network_mnist/network_inst/layer1_inst/weight_n5
add wave -noupdate -group layer_1 -group weight /tb_network_mnist/network_inst/layer1_inst/weight_n6
add wave -noupdate -group layer_1 -group weight /tb_network_mnist/network_inst/layer1_inst/weight_n7
add wave -noupdate -group layer_1 -group weight /tb_network_mnist/network_inst/layer1_inst/weight_n8
add wave -noupdate -group layer_1 -group weight /tb_network_mnist/network_inst/layer1_inst/weight_n9
add wave -noupdate -group layer_1 -group wi /tb_network_mnist/network_inst/layer1_inst/wi_n0
add wave -noupdate -group layer_1 -group wi /tb_network_mnist/network_inst/layer1_inst/wi_n1
add wave -noupdate -group layer_1 -group wi /tb_network_mnist/network_inst/layer1_inst/wi_n2
add wave -noupdate -group layer_1 -group wi /tb_network_mnist/network_inst/layer1_inst/wi_n3
add wave -noupdate -group layer_1 -group wi /tb_network_mnist/network_inst/layer1_inst/wi_n4
add wave -noupdate -group layer_1 -group wi /tb_network_mnist/network_inst/layer1_inst/wi_n5
add wave -noupdate -group layer_1 -group wi /tb_network_mnist/network_inst/layer1_inst/wi_n6
add wave -noupdate -group layer_1 -group wi /tb_network_mnist/network_inst/layer1_inst/wi_n7
add wave -noupdate -group layer_1 -group wi /tb_network_mnist/network_inst/layer1_inst/wi_n8
add wave -noupdate -group layer_1 -group wi /tb_network_mnist/network_inst/layer1_inst/wi_n9
add wave -noupdate -group layer_1 -group overflow /tb_network_mnist/network_inst/layer1_inst/overflow0
add wave -noupdate -group layer_1 -group overflow /tb_network_mnist/network_inst/layer1_inst/overflow1
add wave -noupdate -group layer_1 -group overflow /tb_network_mnist/network_inst/layer1_inst/overflow2
add wave -noupdate -group layer_1 -group overflow /tb_network_mnist/network_inst/layer1_inst/overflow3
add wave -noupdate -group layer_1 -group overflow /tb_network_mnist/network_inst/layer1_inst/overflow4
add wave -noupdate -group layer_1 -group overflow /tb_network_mnist/network_inst/layer1_inst/overflow5
add wave -noupdate -group layer_1 -group overflow /tb_network_mnist/network_inst/layer1_inst/overflow6
add wave -noupdate -group layer_1 -group overflow /tb_network_mnist/network_inst/layer1_inst/overflow7
add wave -noupdate -group layer_1 -group overflow /tb_network_mnist/network_inst/layer1_inst/overflow8
add wave -noupdate -group layer_1 -group overflow /tb_network_mnist/network_inst/layer1_inst/overflow9
add wave -noupdate -group layer_1 -expand -group acc -radix binary /tb_network_mnist/network_inst/layer1_inst/acc_en
add wave -noupdate -group layer_1 -expand -group acc /tb_network_mnist/network_inst/layer1_inst/acc_n0
add wave -noupdate -group layer_1 -expand -group acc /tb_network_mnist/network_inst/layer1_inst/acc_n1
add wave -noupdate -group layer_1 -expand -group acc /tb_network_mnist/network_inst/layer1_inst/acc_n2
add wave -noupdate -group layer_1 -expand -group acc /tb_network_mnist/network_inst/layer1_inst/acc_n3
add wave -noupdate -group layer_1 -expand -group acc /tb_network_mnist/network_inst/layer1_inst/acc_n4
add wave -noupdate -group layer_1 -expand -group acc /tb_network_mnist/network_inst/layer1_inst/acc_n5
add wave -noupdate -group layer_1 -expand -group acc /tb_network_mnist/network_inst/layer1_inst/acc_n6
add wave -noupdate -group layer_1 -expand -group acc /tb_network_mnist/network_inst/layer1_inst/acc_n7
add wave -noupdate -group layer_1 -expand -group acc /tb_network_mnist/network_inst/layer1_inst/acc_n8
add wave -noupdate -group layer_1 -expand -group acc -radix binary /tb_network_mnist/network_inst/layer1_inst/acc_n9
add wave -noupdate -group layer_1 -group msb_acc /tb_network_mnist/network_inst/layer1_inst/msb_acc_n0
add wave -noupdate -group layer_1 -group msb_acc /tb_network_mnist/network_inst/layer1_inst/msb_acc_n1
add wave -noupdate -group layer_1 -group msb_acc /tb_network_mnist/network_inst/layer1_inst/msb_acc_n2
add wave -noupdate -group layer_1 -group msb_acc /tb_network_mnist/network_inst/layer1_inst/msb_acc_n3
add wave -noupdate -group layer_1 -group msb_acc /tb_network_mnist/network_inst/layer1_inst/msb_acc_n4
add wave -noupdate -group layer_1 -group msb_acc /tb_network_mnist/network_inst/layer1_inst/msb_acc_n5
add wave -noupdate -group layer_1 -group msb_acc /tb_network_mnist/network_inst/layer1_inst/msb_acc_n6
add wave -noupdate -group layer_1 -group msb_acc /tb_network_mnist/network_inst/layer1_inst/msb_acc_n7
add wave -noupdate -group layer_1 -group msb_acc /tb_network_mnist/network_inst/layer1_inst/msb_acc_n8
add wave -noupdate -group layer_1 -group msb_acc /tb_network_mnist/network_inst/layer1_inst/msb_acc_n9
add wave -noupdate -group layer_1 -group trunc_acc /tb_network_mnist/network_inst/layer1_inst/trunc_acc_n0
add wave -noupdate -group layer_1 -group trunc_acc /tb_network_mnist/network_inst/layer1_inst/trunc_acc_n1
add wave -noupdate -group layer_1 -group trunc_acc /tb_network_mnist/network_inst/layer1_inst/trunc_acc_n2
add wave -noupdate -group layer_1 -group trunc_acc /tb_network_mnist/network_inst/layer1_inst/trunc_acc_n3
add wave -noupdate -group layer_1 -group trunc_acc /tb_network_mnist/network_inst/layer1_inst/trunc_acc_n4
add wave -noupdate -group layer_1 -group trunc_acc /tb_network_mnist/network_inst/layer1_inst/trunc_acc_n5
add wave -noupdate -group layer_1 -group trunc_acc /tb_network_mnist/network_inst/layer1_inst/trunc_acc_n6
add wave -noupdate -group layer_1 -group trunc_acc /tb_network_mnist/network_inst/layer1_inst/trunc_acc_n7
add wave -noupdate -group layer_1 -group trunc_acc /tb_network_mnist/network_inst/layer1_inst/trunc_acc_n8
add wave -noupdate -group layer_1 -group trunc_acc /tb_network_mnist/network_inst/layer1_inst/trunc_acc_n9
add wave -noupdate -group layer_1 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer1_inst/index_n0
add wave -noupdate -group layer_1 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer1_inst/index_n1
add wave -noupdate -group layer_1 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer1_inst/index_n2
add wave -noupdate -group layer_1 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer1_inst/index_n3
add wave -noupdate -group layer_1 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer1_inst/index_n4
add wave -noupdate -group layer_1 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer1_inst/index_n5
add wave -noupdate -group layer_1 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer1_inst/index_n6
add wave -noupdate -group layer_1 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer1_inst/index_n7
add wave -noupdate -group layer_1 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer1_inst/index_n8
add wave -noupdate -group layer_1 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer1_inst/index_n9
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfpos_trunc_acc_n0
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfpos_trunc_acc_n1
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfpos_trunc_acc_n2
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfpos_trunc_acc_n3
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfpos_trunc_acc_n4
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfpos_trunc_acc_n5
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfpos_trunc_acc_n6
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfpos_trunc_acc_n7
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfpos_trunc_acc_n8
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfpos_trunc_acc_n9
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfneg_trunc_acc_n0
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfneg_trunc_acc_n1
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfneg_trunc_acc_n2
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfneg_trunc_acc_n3
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfneg_trunc_acc_n4
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfneg_trunc_acc_n5
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfneg_trunc_acc_n6
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfneg_trunc_acc_n7
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfneg_trunc_acc_n8
add wave -noupdate -group layer_1 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer1_inst/ovfneg_trunc_acc_n9
add wave -noupdate -group layer_1 -expand -group mem -radix unsigned /tb_network_mnist/network_inst/layer1_inst/mem_index
add wave -noupdate -group layer_1 -expand -group mem /tb_network_mnist/network_inst/layer1_inst/mem_rd_en
add wave -noupdate -group layer_1 -expand -group mem -radix unsigned /tb_network_mnist/network_inst/layer1_inst/mem_out
add wave -noupdate -group layer_1 -expand -group mem -radix unsigned /tb_network_mnist/network_inst/layer1_inst/mem_last_state
add wave -noupdate -group layer_1 -expand -group mem -radix unsigned /tb_network_mnist/network_inst/layer1_inst/mem_state
add wave -noupdate -group layer_1 -expand -group mem -radix unsigned /tb_network_mnist/network_inst/layer1_inst/mem_next_state
add wave -noupdate -group layer_2 -radix binary /tb_network_mnist/network_inst/layer2_inst/valid_in
add wave -noupdate -group layer_2 -radix unsigned /tb_network_mnist/network_inst/layer2_inst/layer_in
add wave -noupdate -group layer_2 /tb_network_mnist/network_inst/layer2_inst/layer_in_ext
add wave -noupdate -group layer_2 /tb_network_mnist/network_inst/layer2_inst/layer_out
add wave -noupdate -group layer_2 /tb_network_mnist/network_inst/layer2_inst/valid_out
add wave -noupdate -group layer_2 -expand -group FSM -radix unsigned /tb_network_mnist/network_inst/layer2_inst/state
add wave -noupdate -group layer_2 -expand -group FSM -radix unsigned /tb_network_mnist/network_inst/layer2_inst/next_state
add wave -noupdate -group layer_2 -group weight /tb_network_mnist/network_inst/layer2_inst/weight_n0
add wave -noupdate -group layer_2 -group weight /tb_network_mnist/network_inst/layer2_inst/weight_n1
add wave -noupdate -group layer_2 -group weight /tb_network_mnist/network_inst/layer2_inst/weight_n2
add wave -noupdate -group layer_2 -group weight /tb_network_mnist/network_inst/layer2_inst/weight_n3
add wave -noupdate -group layer_2 -group weight /tb_network_mnist/network_inst/layer2_inst/weight_n4
add wave -noupdate -group layer_2 -group weight /tb_network_mnist/network_inst/layer2_inst/weight_n5
add wave -noupdate -group layer_2 -group weight /tb_network_mnist/network_inst/layer2_inst/weight_n6
add wave -noupdate -group layer_2 -group weight /tb_network_mnist/network_inst/layer2_inst/weight_n7
add wave -noupdate -group layer_2 -group weight /tb_network_mnist/network_inst/layer2_inst/weight_n8
add wave -noupdate -group layer_2 -group weight /tb_network_mnist/network_inst/layer2_inst/weight_n9
add wave -noupdate -group layer_2 -group wi /tb_network_mnist/network_inst/layer2_inst/wi_n0
add wave -noupdate -group layer_2 -group wi /tb_network_mnist/network_inst/layer2_inst/wi_n1
add wave -noupdate -group layer_2 -group wi /tb_network_mnist/network_inst/layer2_inst/wi_n2
add wave -noupdate -group layer_2 -group wi /tb_network_mnist/network_inst/layer2_inst/wi_n3
add wave -noupdate -group layer_2 -group wi /tb_network_mnist/network_inst/layer2_inst/wi_n4
add wave -noupdate -group layer_2 -group wi /tb_network_mnist/network_inst/layer2_inst/wi_n5
add wave -noupdate -group layer_2 -group wi /tb_network_mnist/network_inst/layer2_inst/wi_n6
add wave -noupdate -group layer_2 -group wi /tb_network_mnist/network_inst/layer2_inst/wi_n7
add wave -noupdate -group layer_2 -group wi /tb_network_mnist/network_inst/layer2_inst/wi_n8
add wave -noupdate -group layer_2 -group wi /tb_network_mnist/network_inst/layer2_inst/wi_n9
add wave -noupdate -group layer_2 -group overflow /tb_network_mnist/network_inst/layer2_inst/overflow0
add wave -noupdate -group layer_2 -group overflow /tb_network_mnist/network_inst/layer2_inst/overflow1
add wave -noupdate -group layer_2 -group overflow /tb_network_mnist/network_inst/layer2_inst/overflow2
add wave -noupdate -group layer_2 -group overflow /tb_network_mnist/network_inst/layer2_inst/overflow3
add wave -noupdate -group layer_2 -group overflow /tb_network_mnist/network_inst/layer2_inst/overflow4
add wave -noupdate -group layer_2 -group overflow /tb_network_mnist/network_inst/layer2_inst/overflow5
add wave -noupdate -group layer_2 -group overflow /tb_network_mnist/network_inst/layer2_inst/overflow6
add wave -noupdate -group layer_2 -group overflow /tb_network_mnist/network_inst/layer2_inst/overflow7
add wave -noupdate -group layer_2 -group overflow /tb_network_mnist/network_inst/layer2_inst/overflow8
add wave -noupdate -group layer_2 -group overflow /tb_network_mnist/network_inst/layer2_inst/overflow9
add wave -noupdate -group layer_2 -expand -group acc /tb_network_mnist/network_inst/layer2_inst/acc_en
add wave -noupdate -group layer_2 -expand -group acc /tb_network_mnist/network_inst/layer2_inst/acc_n0
add wave -noupdate -group layer_2 -expand -group acc /tb_network_mnist/network_inst/layer2_inst/acc_n1
add wave -noupdate -group layer_2 -expand -group acc /tb_network_mnist/network_inst/layer2_inst/acc_n2
add wave -noupdate -group layer_2 -expand -group acc /tb_network_mnist/network_inst/layer2_inst/acc_n3
add wave -noupdate -group layer_2 -expand -group acc /tb_network_mnist/network_inst/layer2_inst/acc_n4
add wave -noupdate -group layer_2 -expand -group acc /tb_network_mnist/network_inst/layer2_inst/acc_n5
add wave -noupdate -group layer_2 -expand -group acc /tb_network_mnist/network_inst/layer2_inst/acc_n6
add wave -noupdate -group layer_2 -expand -group acc /tb_network_mnist/network_inst/layer2_inst/acc_n7
add wave -noupdate -group layer_2 -expand -group acc /tb_network_mnist/network_inst/layer2_inst/acc_n8
add wave -noupdate -group layer_2 -expand -group acc -radix unsigned /tb_network_mnist/network_inst/layer2_inst/acc_n9
add wave -noupdate -group layer_2 -group msb_acc /tb_network_mnist/network_inst/layer2_inst/msb_acc_n0
add wave -noupdate -group layer_2 -group msb_acc /tb_network_mnist/network_inst/layer2_inst/msb_acc_n1
add wave -noupdate -group layer_2 -group msb_acc /tb_network_mnist/network_inst/layer2_inst/msb_acc_n2
add wave -noupdate -group layer_2 -group msb_acc /tb_network_mnist/network_inst/layer2_inst/msb_acc_n3
add wave -noupdate -group layer_2 -group msb_acc /tb_network_mnist/network_inst/layer2_inst/msb_acc_n4
add wave -noupdate -group layer_2 -group msb_acc /tb_network_mnist/network_inst/layer2_inst/msb_acc_n5
add wave -noupdate -group layer_2 -group msb_acc /tb_network_mnist/network_inst/layer2_inst/msb_acc_n6
add wave -noupdate -group layer_2 -group msb_acc /tb_network_mnist/network_inst/layer2_inst/msb_acc_n7
add wave -noupdate -group layer_2 -group msb_acc /tb_network_mnist/network_inst/layer2_inst/msb_acc_n8
add wave -noupdate -group layer_2 -group msb_acc /tb_network_mnist/network_inst/layer2_inst/msb_acc_n9
add wave -noupdate -group layer_2 -group trunc_acc /tb_network_mnist/network_inst/layer2_inst/trunc_acc_n0
add wave -noupdate -group layer_2 -group trunc_acc /tb_network_mnist/network_inst/layer2_inst/trunc_acc_n1
add wave -noupdate -group layer_2 -group trunc_acc /tb_network_mnist/network_inst/layer2_inst/trunc_acc_n2
add wave -noupdate -group layer_2 -group trunc_acc /tb_network_mnist/network_inst/layer2_inst/trunc_acc_n3
add wave -noupdate -group layer_2 -group trunc_acc /tb_network_mnist/network_inst/layer2_inst/trunc_acc_n4
add wave -noupdate -group layer_2 -group trunc_acc /tb_network_mnist/network_inst/layer2_inst/trunc_acc_n5
add wave -noupdate -group layer_2 -group trunc_acc /tb_network_mnist/network_inst/layer2_inst/trunc_acc_n6
add wave -noupdate -group layer_2 -group trunc_acc /tb_network_mnist/network_inst/layer2_inst/trunc_acc_n7
add wave -noupdate -group layer_2 -group trunc_acc /tb_network_mnist/network_inst/layer2_inst/trunc_acc_n8
add wave -noupdate -group layer_2 -group trunc_acc /tb_network_mnist/network_inst/layer2_inst/trunc_acc_n9
add wave -noupdate -group layer_2 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer2_inst/index_n0
add wave -noupdate -group layer_2 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer2_inst/index_n1
add wave -noupdate -group layer_2 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer2_inst/index_n2
add wave -noupdate -group layer_2 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer2_inst/index_n3
add wave -noupdate -group layer_2 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer2_inst/index_n4
add wave -noupdate -group layer_2 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer2_inst/index_n5
add wave -noupdate -group layer_2 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer2_inst/index_n6
add wave -noupdate -group layer_2 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer2_inst/index_n7
add wave -noupdate -group layer_2 -expand -group index -radix unsigned /tb_network_mnist/network_inst/layer2_inst/index_n8
add wave -noupdate -group layer_2 -expand -group index -radix binary /tb_network_mnist/network_inst/layer2_inst/index_n9
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfpos_trunc_acc_n0
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfpos_trunc_acc_n1
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfpos_trunc_acc_n2
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfpos_trunc_acc_n3
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfpos_trunc_acc_n4
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfpos_trunc_acc_n5
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfpos_trunc_acc_n6
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfpos_trunc_acc_n7
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfpos_trunc_acc_n8
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfpos_trunc_acc_n9
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfneg_trunc_acc_n0
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfneg_trunc_acc_n1
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfneg_trunc_acc_n2
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfneg_trunc_acc_n3
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfneg_trunc_acc_n4
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfneg_trunc_acc_n5
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfneg_trunc_acc_n6
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfneg_trunc_acc_n7
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfneg_trunc_acc_n8
add wave -noupdate -group layer_2 -group overflow_pos_neg -radix binary /tb_network_mnist/network_inst/layer2_inst/ovfneg_trunc_acc_n9
add wave -noupdate -group layer_2 -expand -group mem -radix unsigned /tb_network_mnist/network_inst/layer2_inst/mem_index
add wave -noupdate -group layer_2 -expand -group mem -radix binary /tb_network_mnist/network_inst/layer2_inst/mem_rd_en
add wave -noupdate -group layer_2 -expand -group mem -radix unsigned /tb_network_mnist/network_inst/layer2_inst/mem_out
add wave -noupdate -group layer_2 -expand -group mem -radix unsigned /tb_network_mnist/network_inst/layer2_inst/mem_last_state
add wave -noupdate -group layer_2 -expand -group mem -radix unsigned /tb_network_mnist/network_inst/layer2_inst/mem_state
add wave -noupdate -group layer_2 -expand -group mem -radix unsigned /tb_network_mnist/network_inst/layer2_inst/mem_next_state
add wave -noupdate -expand -group Hardmax -radix binary /tb_network_mnist/network_inst/hardmax_inst/start
add wave -noupdate -expand -group Hardmax -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/data_in
add wave -noupdate -expand -group Hardmax -expand -group reg -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/reg_0
add wave -noupdate -expand -group Hardmax -expand -group reg -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/reg_1
add wave -noupdate -expand -group Hardmax -expand -group reg -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/reg_2
add wave -noupdate -expand -group Hardmax -expand -group reg -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/reg_3
add wave -noupdate -expand -group Hardmax -expand -group reg -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/reg_4
add wave -noupdate -expand -group Hardmax -expand -group reg -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/reg_5
add wave -noupdate -expand -group Hardmax -expand -group reg -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/reg_6
add wave -noupdate -expand -group Hardmax -expand -group reg -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/reg_7
add wave -noupdate -expand -group Hardmax -expand -group reg -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/reg_8
add wave -noupdate -expand -group Hardmax -expand -group reg -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/reg_9
add wave -noupdate -expand -group Hardmax -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/state
add wave -noupdate -expand -group Hardmax -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/next_state
add wave -noupdate -expand -group Hardmax -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/max
add wave -noupdate -expand -group Hardmax -radix unsigned /tb_network_mnist/network_inst/hardmax_inst/index
add wave -noupdate -expand -group Hardmax -radix binary /tb_network_mnist/network_inst/hardmax_inst/ready
add wave -noupdate -expand -group activation_ROM /tb_network_mnist/network_inst/activationROM_2048x9bits_inst/address
add wave -noupdate -expand -group activation_ROM /tb_network_mnist/network_inst/activationROM_2048x9bits_inst/clock
add wave -noupdate -expand -group activation_ROM /tb_network_mnist/network_inst/activationROM_2048x9bits_inst/rden
add wave -noupdate -expand -group activation_ROM /tb_network_mnist/network_inst/activationROM_2048x9bits_inst/q
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {13906828 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 228
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {17072714 ps} {17224921 ps}
