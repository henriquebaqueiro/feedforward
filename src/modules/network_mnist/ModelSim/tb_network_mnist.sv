`timescale 1ns/100ps
module tb_network_mnist;

parameter WHOLE = 2;
parameter FRACT = 13;
parameter LUT_RES = 9; // LUT resolution (n of bits activation function will be represented)

parameter PIX_RES = 8; // Pixel resolution. MNIST is 8.
parameter F = (2.0**-FRACT);
// choose between 10, 25, 50, 500 or 10000 samples
// integer number_of_images = 1;
// integer number_of_images = 10; 
// integer number_of_images = 25;
integer number_of_images = 50;
// integer number_of_images = 500;
// integer number_of_images = 10000;


reg clk;
initial clk = 1'b0;
always #(1) clk = ~clk;

reg rst_n;
initial begin
  rst_n = 1;
  @(negedge clk);
  rst_n = 0;
  @(negedge clk);
  rst_n = 1;
end

reg valid_in;
wire valid_out;
integer fwd_passes;
initial begin
  valid_in = 1'b0;
  @(negedge clk);

  for (fwd_passes=0; fwd_passes<number_of_images; fwd_passes=fwd_passes+1) begin
    @(negedge clk);
    valid_in = 1'b1;
    @(negedge clk);
    valid_in = 1'b0;
    @(negedge valid_out); // when 1 forward pass is complete
  end
  
  $stop;
end


// ------------ LOGIC FOR COUNTING CLOCK CYCLES -----------
// --------------------------------------------------------

/*  reg last_valid_in;
 wire valid_in_rise = valid_in && !last_valid_in;
 always@(posedge clk or negedge rst_n) begin
   if (!rst_n) begin
     last_valid_in <= 1'b0;
   end
   else begin
     last_valid_in <= valid_in;
   end
 end

 reg [31:0] clk_counter;
 always@(posedge clk or negedge rst_n) begin
   if (!rst_n) begin
     clk_counter <= 32'b0;
   end
   else begin
     if (valid_in_rise) begin
       clk_counter <= 32'b0;
     end
     else begin
       clk_counter <= clk_counter + 1'b1;
     end
   end
 end */

// --------- END LOGIC FOR COUNTING CLOCK CYCLES ----------
// --------------------------------------------------------


// ------------------------------------------------------------------------------------------
// ----- LOGIC TO COUNT WHICH PIXELS SHOULD BE AT THE NETWORK INPUT (based on valid_in) -----

// integer img_counter = 0;
// bit x = 0; // img_counter should start counting from 0
// always @(posedge valid_in) begin
//   if (!x) begin
//     x <= 1'b1;
//     img_counter <= 32'd0;
//   end
//   else img_counter = img_counter + 1'b1;
// end

// --------------------- END LOGIC TO COUNT IMAGES AND PIXELS ------------------------------
// -----------------------------------------------------------------------------------------

function string get_path_from_file(string fullpath_filename);
    int i;
    int str_index;
    logic found_path;
    static string ret="";

    for (i = fullpath_filename.len()-1; i>0; i=i-1) begin
        if (fullpath_filename[i] == "/") begin
            found_path=1;
            str_index=i;
            break;
        end
    end
    if (found_path==1) begin
        ret=fullpath_filename.substr(0,str_index);
    end else begin
       // `uvm_error("pve_get_path_from_file-1", $sformatf("Not found a valid path for this file: %s",fullpath_filename));
    end

    return ret;
endfunction

// -----------------------------------------------------
// ------------------ VERIFICATION ---------------------

// LOAD DATASET PIXELS AND LABLES FILES INTO TESTBENCH VARIABLES

integer lable_file, imgs_file;
string testset_pixels_path;
initial begin

  lable_file = $fopen("/home/baqueiro/feedforward/src/data/testset_labels.txt", "r");
  if (lable_file == 0)  $display("TESTBENCH: Lables file handle was NULL");
  else                  $display("TESTBENCH: Lables file loaded successfully");

  case (number_of_images)
    1 :       testset_pixels_path = "/home/baqueiro/feedforward/src/data/testset_pixels_1.txt";
    10 :      testset_pixels_path = "/home/baqueiro/feedforward/src/data/testset_pixels_10.txt";
    25 :      testset_pixels_path = "/home/baqueiro/feedforward/src/data/testset_pixels_25.txt";
    50 :      testset_pixels_path = "/home/baqueiro/feedforward/src/data/testset_pixels_50.txt";
    500 :     testset_pixels_path = "/home/baqueiro/feedforward/src/data/testset_pixels_500.txt";
    10000 :   testset_pixels_path = "/home/baqueiro/feedforward/src/data/testset_pixels_10000.txt";
    default : testset_pixels_path = "/home/baqueiro/feedforward/src/data/testset_pixels_10000.txt";
  endcase

  imgs_file  = $fopen(testset_pixels_path, "r");
  if (imgs_file == 0) $display("TESTBENCH: Images file handle was NULL");
  else                $display("TESTBENCH: Images file loaded successfully");

end

integer current_index, current_label;
integer tmp_label, tmp_imgs;
always @(posedge clk) begin
  if (!$feof(lable_file)) begin
    if (valid_in) begin
      tmp_label = $fscanf(lable_file, "%d%d", current_index, current_label);
    end
  end
  else $fclose(lable_file);
end

// ELABORATE THE LOGIC TO THE IMAGES PARSING
integer file_pixel_counter = 0;
integer file_pixel [0:7840000-1]; // this variable stores all pixels from all images
real progress = 0;
real last_displayed_progress = 0;
string path;
initial begin
  while (!$feof(imgs_file)) begin
    progress = (100*(real'(file_pixel_counter))/(784*number_of_images));

    if (progress - last_displayed_progress >= 0.1) begin
      $display("Loading file into ModelSim (%.1f%%)", progress);
      last_displayed_progress = progress;
    end

    tmp_imgs = $fscanf(imgs_file, "%f", file_pixel[file_pixel_counter]);
    file_pixel_counter+=1;
  end

  path=get_path_from_file(`__FILE__);
  $display(path);

  $fclose(imgs_file);
end

// DISPLAY NETWORK CLASSIFICATIONS VERSUS TEST_SET LABLES 
string result;
real forward_passes = 0;
real correct_classification = 0;
wire [3:0] net_classification;
always @(posedge valid_out) begin // delayed_valid_out

  if (net_classification == current_label) begin
    result = "OK";
    correct_classification += 1;
  end
  else begin
    result = "X";
  end

  $display("[%1d] Verilog ANN: %1d | MNIST_label: %1d (%s)", forward_passes, net_classification, current_label, result);
  forward_passes+=1;
  if (forward_passes == number_of_images) begin
    $display("\n##### CLASSIFICATION ACCURACY: %.2f%% (%1d/%1d) #####\n", 
    100.0*correct_classification/forward_passes, correct_classification, forward_passes);
  end
end

// ---------------- END VERIFICATION -------------------
// -----------------------------------------------------


// ------------------ PROVIDE INPUT DATA ------------------
// --------------------------------------------------------

/* ... from memory, given a pre-loaded .hex file */
// reg [PIX_RES-1:0] pixel;
// imgROM_rasterscan imgROM_rasterscan_inst (
//   .clk(clk),
//   .rst_n(rst_n),
//   .valid_in(valid_in),
//   .q(pixel),
//   .q_addr(), // wire [9:0] addr;
//   .valid_content() // wire valid_content; 
// );

reg [9:0] delayed_1st_layer_state; // size may vary depending on number of network inputs
always @(posedge clk or negedge rst_n) begin
  if (!rst_n) delayed_1st_layer_state <= 10'd0;
  else        delayed_1st_layer_state <= network_inst.layer1_inst.state;
end
/* ... from file, using file_pixel[] array */
/////////////////////////////////////// REVISE THIS, ALL CASES ///////////////////////////////////////////////
reg [31:0] file_pixel_index;
wire [LUT_RES-1:0] net_in;
generate
  if(LUT_RES >= PIX_RES) assign net_in = {file_pixel[file_pixel_index], {(LUT_RES-PIX_RES){1'b0}}};
  else                  assign net_in =  file_pixel[file_pixel_index][PIX_RES-1:(PIX_RES-LUT_RES)];
endgenerate
always @(network_inst.layer1_inst.state) begin
  file_pixel_index = current_index*784 + delayed_1st_layer_state;

/*   if   (LUT_RES > PIX_RES) net_in = {file_pixel[file_pixel_index], {(LUT_RES-PIX_RES){1'b0}}};
  else                     net_in =  file_pixel[file_pixel_index][PIX_RES-1:(PIX_RES-LUT_RES)]; */
end

// -------------------- END INPUT DATA --------------------
// --------------------------------------------------------


// ----------------- NETWORK INSTANTIATION -----------------
// ---------------------------------------------------------

network #(
  .FRACT(FRACT),
  .WHOLE(WHOLE),
  .N(LUT_RES)
)
network_inst
(
	.clk(clk),
  .rst_n(rst_n),
  .net_in(net_in),
  .valid_in(valid_in),
 	.net_classification(net_classification),
  .net_valid_out(valid_out)
);

// -------------- END NETWORK INSTANTIATION ---------------
// --------------------------------------------------------


// ---------------------- REAL VALUES FOR DEBUG ----------------------
// -------------------------------------------------------------------

function real sigmoid(input real z);
  sigmoid = 1.0/(1.0+ 2.718281828459**(-z));
endfunction

// real net_acc_l1_r [0:9], net_acc_l2_r [0:9];
// always @(*) begin
//   net_acc_l1_r[0] = network_inst.layer1_inst.acc_n0 * F;
//   net_acc_l1_r[1] = network_inst.layer1_inst.acc_n1 * F;
//   net_acc_l1_r[2] = network_inst.layer1_inst.acc_n2 * F;
//   net_acc_l1_r[3] = network_inst.layer1_inst.acc_n3 * F;
//   net_acc_l1_r[4] = network_inst.layer1_inst.acc_n4 * F;
//   net_acc_l1_r[5] = network_inst.layer1_inst.acc_n5 * F;
//   net_acc_l1_r[6] = network_inst.layer1_inst.acc_n6 * F;
//   net_acc_l1_r[7] = network_inst.layer1_inst.acc_n7 * F;
//   net_acc_l1_r[8] = network_inst.layer1_inst.acc_n8 * F;
//   net_acc_l1_r[9] = network_inst.layer1_inst.acc_n9 * F;

//   net_acc_l2_r[0] = network_inst.layer2_inst.acc_n0 * F;
//   net_acc_l2_r[1] = network_inst.layer2_inst.acc_n1 * F;
//   net_acc_l2_r[2] = network_inst.layer2_inst.acc_n2 * F;
//   net_acc_l2_r[3] = network_inst.layer2_inst.acc_n3 * F;
//   net_acc_l2_r[4] = network_inst.layer2_inst.acc_n4 * F;
//   net_acc_l2_r[5] = network_inst.layer2_inst.acc_n5 * F;
//   net_acc_l2_r[6] = network_inst.layer2_inst.acc_n6 * F;
//   net_acc_l2_r[7] = network_inst.layer2_inst.acc_n7 * F;
//   net_acc_l2_r[8] = network_inst.layer2_inst.acc_n8 * F;
//   net_acc_l2_r[9] = network_inst.layer2_inst.acc_n9 * F;
// end

integer i;
real file_pixel_r [0:7840000-1];

initial begin
  for (i=0; i<784; i++) begin
    file_pixel_r[i] = file_pixel[i] * (2.0 ** -PIX_RES);
  end
end


integer j, k, l;
real acc_l1_r [0:9];
real weight_l1_r [0:9][0:783];
real weighted_input_l1_r;
integer n_cnt, w_cnt;
initial begin

  @(negedge rst_n);

  for (j=0; j<10; j++) begin
    acc_l1_r[j] = network_inst.layer1_inst.bias[j] * F; // ok!
  end

  for (n_cnt=0; n_cnt<10; n_cnt++) begin
    for (w_cnt=0; w_cnt<784; w_cnt++) begin
      weight_l1_r[n_cnt][w_cnt] = network_inst.layer1_inst.weight[n_cnt][w_cnt] * F;
    end
  end

  // for (k=0; k<784; k++) begin
  //   weight_l1_r[0][k] = network_inst.layer1_inst.weight[0][k] * F; // ok!
  // end

  repeat (5) @(posedge clk); //?

  for (l=0; l<784; l++) begin
    weighted_input_l1_r = file_pixel_r[l] * weight_l1_r[0][l]; // just neuron 0
    acc_l1_r[0] += weighted_input_l1_r;
    @(posedge clk); // just to compare with network's accumulator
  end


end

// --------------------  END REAL VALUES FOR DEBUG -------------------
// -------------------------------------------------------------------

endmodule
